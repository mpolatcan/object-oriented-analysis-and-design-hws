package part2;

/**
 * Created by mpolatcan-gyte_cse on 27.11.2016.
 */
public class ClusterVector {
    private int x;
    private int y;
    private int z;

    public ClusterVector() {

    }

    public ClusterVector(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setZ(int z) {
        this.z = z;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    @Override
    public String toString() {
        return String.format("[%d,%d,%d]", x, y, z);
    }
}
