package part2;

/**
 * Created by mpolatcan-gyte_cse on 28.11.2016.
 */
public interface DistanceMethod {
    double calculateDistance(ClusterVector vector, ClusterVector centroid);
}
