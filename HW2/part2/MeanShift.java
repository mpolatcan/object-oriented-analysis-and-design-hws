package part2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by mpolatcan-gyte_cse on 27.11.2016.
 */
public class MeanShift extends Clustering{
    private static final double EPSILON = 0.1;
    private double kernelBandwidth;
    private List<Cluster> shiftedPoints;


    public MeanShift() {

    }

    @Override
    public void initializeParameters() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter kernel bandwith: ");
        kernelBandwidth = scanner.nextDouble();
    }

    @Override
    public void clusteringAlgorithm() {
        List<List<Integer>> points = new ArrayList<List<Integer>>();

        for (int i = 0; i < data.size(); ++i) {
            points.add(new ArrayList<Integer>());
            points.get(i).add(data.get(i).getX());
            points.get(i).add(data.get(i).getY());
            points.get(i).add(data.get(i).getZ());
        }

       shiftedPoints = cluster(points,kernelBandwidth);
    }

    @Override
    public void printClusters() {
        System.out.printf("For MeanShift clusters are: ");
        for (int i = 0; i < shiftedPoints.size(); i++) {
            System.out.printf(shiftedPoints.get(i).mode.toString());
        }
    }

    private List<List<Integer>> meanShift(List<List<Integer>> points, double kernelBandwidth) {
        List<Boolean> stopMoving = new ArrayList<Boolean>();
        List<Boolean> allStopped = new ArrayList<Boolean>();
        List<List<Integer>> shiftedPoints = new ArrayList<List<Integer>>();
        shiftedPoints.addAll(points);

        for (int i = 0; i < points.size(); i++) {
            stopMoving.add(new Boolean(false));
            allStopped.add(new Boolean(true));
        }

        double maxShiftDistance;
        do {
            maxShiftDistance = 0;

            for (int i = 0; i < shiftedPoints.size(); i++) {
                if (!stopMoving.get(i)) {
                    List<Integer> newPoint = shiftPoint(shiftedPoints.get(i), points, kernelBandwidth);
                    double shiftDistance = calculateDistance(newPoint,shiftedPoints.get(i));

                    if (shiftDistance > maxShiftDistance) {
                        maxShiftDistance = shiftDistance;
                    }

                    if (shiftDistance <= EPSILON) {
                        stopMoving.set(i,true);
                    }

                    shiftedPoints.set(i,newPoint);
                }
                System.out.printf("Max shift distance: %f\n", maxShiftDistance);
            }
        } while (!stopMoving.containsAll(allStopped));

        return shiftedPoints;
    }

    private List<Integer> shiftPoint(List<Integer> point, List<List<Integer>> points, double kernelBandwidth) {
        List<Integer> shiftedPoint = new ArrayList<Integer>();
        shiftedPoint.addAll(point);

        for (int i = 0; i < shiftedPoint.size(); ++i) {
            shiftedPoint.set(i,0);
        }

        double totalWeight = 0;

        for (int j = 0; j < points.size(); j++) {
            List<Integer> tempPoint = points.get(j);
            double distance = calculateDistance(point,tempPoint);
            double weight = gaussianKernel(distance,kernelBandwidth);

            for (int k = 0; k < shiftedPoint.size(); k++) {
                shiftedPoint.set(k, ((int) (shiftedPoint.get(k) + (tempPoint.get(k) * weight))));
            }

            totalWeight += weight;
        }

        for (int i = 0; i < shiftedPoint.size(); i++) {
            shiftedPoint.set(i, ((int) (shiftedPoint.get(i) / totalWeight)));
        }

        return shiftedPoint;
    }

    private List<Cluster> cluster(List<List<Integer>> points, double kernelBandwidth) {
        List<List<Integer>> shiftedPoints = meanShift(points,kernelBandwidth);
        return clusterPoints(points,shiftedPoints);
    }

    private List<Cluster> clusterPoints(List<List<Integer>> points, List<List<Integer>> shiftedPoints) {
        List<Cluster> clusters = new ArrayList<Cluster>();

        for (int i = 0; i < shiftedPoints.size(); i++) {
            int k = 0;

            for (; k < clusters.size(); ++k) {
                if (calculateDistance(shiftedPoints.get(i),clusters.get(i).mode) <= EPSILON) {
                    break;
                }
            }

            if (k == clusters.size()) {
                Cluster cluster = new Cluster();
                cluster.mode = shiftedPoints.get(i);
                clusters.add(cluster);
            }

            clusters.get(i).support.add(points.get(i));
        }

        return clusters;
    }

    private double calculateDistance(List<Integer> firstPoint, List<Integer> secondPoint) {
        double dist = 0;

        for (int i = 0; i < firstPoint.size(); ++i) {
            dist += Math.pow((firstPoint.get(i) - secondPoint.get(i)),2);
        }

        return Math.sqrt(dist);
    }

    private double gaussianKernel(double distance, double kernelBandwidth) {
        double weight = 0;

        weight = Math.exp(-1.0/2.0 * Math.pow(distance,2) / Math.pow(kernelBandwidth,2));

        return weight;
    }

    private class Cluster {
        private List<Integer> mode;
        private List<List<Integer>> support;
    }
}
