package part2;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 * Created by mpolatcan-gyte_cse on 27.11.2016.
 */
public class KMeans extends Clustering {
    private ArrayList<ArrayList<ClusterVector>> centroidClusters;
    private ClusterVector[] centroids;
    private DistanceMethod distanceMethod;
    private double[] distances;
    private int kClusterNum;

    public KMeans() {

    }

    @Override
    public void initializeParameters() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter number of clusters (k parameter): ");
        kClusterNum = scanner.nextInt();

        centroids = new ClusterVector[kClusterNum];
        centroidClusters = new ArrayList<ArrayList<ClusterVector>>();
        distances = new double[kClusterNum];

        for (int i = 0; i < kClusterNum; i++) {
            centroids[i] = new ClusterVector();
            centroidClusters.add(new ArrayList<ClusterVector>());
        }

        distanceMethod = new EuclideanDistance();
    }

    @Override
    public void clusteringAlgorithm() {
        boolean swapped;

        // Random initialization of centroids
        for (int i = 0; i < kClusterNum; ++i) {
            Random random = new Random();
            centroids[i] = data.get(random.nextInt(data.size()));
        }

        do {
            swapped = false;

            // clustering all datas to centroids which is near to them
            for (int i = 0; i < data.size(); ++i) {
                int minDistIndex = 0;

                for (int j = 0; j < kClusterNum; ++j) {
                    distances[j] = distanceMethod.calculateDistance(data.get(i), centroids[j]);

                    if (j == 0) {
                        minDistIndex = j;
                    } else {
                        if (distances[j] < distances[minDistIndex]) {
                            minDistIndex = j;
                        }
                    }
                }

                centroidClusters.get(minDistIndex).add(data.get(i));
            }

            // Calculate new centroids by calculate mean of cluster elements
            for (int i = 0; i < kClusterNum; ++i) {
                int newCentroidX = 0, newCentroidY = 0, newCentroidZ = 0;

                for (ClusterVector vector : centroidClusters.get(i)) {
                    newCentroidX += vector.getX();
                    newCentroidY += vector.getY();
                    newCentroidZ += vector.getZ();
                }

                if (centroidClusters.get(i).size() != 0) {
                    newCentroidX = newCentroidX / centroidClusters.get(i).size();
                    newCentroidY = newCentroidY / centroidClusters.get(i).size();
                    newCentroidZ = newCentroidZ / centroidClusters.get(i).size();

                    if (centroids[i].getX() != newCentroidX ||
                        centroids[i].getY() != newCentroidY ||
                        centroids[i].getZ() != newCentroidZ) {
                        swapped = true;
                    }

                    centroids[i] = new ClusterVector(newCentroidX, newCentroidY, newCentroidZ);
                }
            }

            // Clear clusters
            for (int j = 0; j < kClusterNum; j++) {
                centroidClusters.get(j).clear();
            }
        } while(swapped);
    }

    @Override
    public void printClusters() {
        System.out.println("For kmeans clusters are:");
        for (int i = 0; i < kClusterNum; ++i) {
            System.out.println("" + (i + 1) + "-" + centroids[i]);
        }
    }
}
