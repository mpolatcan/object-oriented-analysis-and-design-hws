package part2;

/**
 * Created by mpolatcan-gyte_cse on 27.11.2016.
 */
public class Part2Test {
    public static void main(String[] args) {
        Clustering clustering = new KMeans();
        clustering.doClustering();

        clustering = new MeanShift();
        clustering.doClustering();
    }
}
