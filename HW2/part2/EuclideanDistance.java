package part2;

/**
 * Created by mpolatcan-gyte_cse on 28.11.2016.
 */
public class EuclideanDistance implements DistanceMethod {
    @Override
    public double calculateDistance(ClusterVector vector, ClusterVector centroid) {
        double distance = 0.0;

        distance = Math.pow((centroid.getX() - vector.getX()),2) +
                   Math.pow((centroid.getY() - vector.getY()),2) +
                   Math.pow((centroid.getZ() - vector.getZ()),2);


        return Math.sqrt(distance);
    }
}
