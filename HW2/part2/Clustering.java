package part2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mpolatcan-gyte_cse on 27.11.2016.
 */
public abstract class Clustering {
    protected List<ClusterVector> data;

    public Clustering() {
        data = new ArrayList<ClusterVector>();
    }

    public void doClustering() {
        readData();
        initializeParameters();
        clusteringAlgorithm();
        printClusters();
    }

    private void readData() {
        try {
            String line;
            String[] values;
            BufferedReader reader = new BufferedReader(new FileReader("data.txt"));

            while ((line = reader.readLine()) != null) {
                values = line.split(", ");

                ClusterVector clusterVector = new ClusterVector();
                clusterVector.setX(Integer.parseInt(values[0]));
                clusterVector.setY(Integer.parseInt(values[1]));
                clusterVector.setZ(Integer.parseInt(values[2]));

                data.add(clusterVector);
            }

            reader.close();
        } catch (FileNotFoundException err) {
            err.printStackTrace();
        } catch (IOException err) {
            err.printStackTrace();
        }
    }

    public abstract void clusteringAlgorithm();

    public abstract void printClusters();

    public abstract void initializeParameters();
}
