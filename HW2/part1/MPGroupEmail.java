package part1;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mpolatcan-gyte_cse on 29.11.2016.
 */
public class MPGroupEmail extends MPEmail {
    private List<MPEmail> emailList;

    public MPGroupEmail(String name, String domain) {
        super(name, domain);
        emailList = new ArrayList<MPEmail>();
    }

    public void addEmail(MPEmail email) {
        emailList.add(email);
    }

    public void removeEmail(MPEmail email) {
        emailList.remove(email);
    }

    public void printInfo() {
        System.out.println(tab + email + ", " + name);
        for (MPEmail email : emailList) {
            email.setTab(tab + "\t");
            email.printInfo();
        }
    }
}
