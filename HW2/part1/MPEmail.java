package part1;

/**
 * Created by mpolatcan-gyte_cse on 24.11.2016.
 */
public abstract class MPEmail {
    protected String name;
    protected String email;
    protected String tab = "";

    public MPEmail(String name, String domain) {
        this.name = name;
        email = name.replaceAll("\\s+","").toLowerCase() + "@" + domain.toLowerCase();
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public void setTab(String tab) { this.tab = tab; }

    public void printInfo() {
        System.out.println(tab + email + ", " + name);
    }
}
