package part1;

/**
 * Created by mpolatcan-gyte_cse on 24.11.2016.
 */
public class Part1Test
{
    public static void main(String[] args) {
       MPGroupEmail gtuEngFac = new MPGroupEmail("Gtu Engineering Faculty", "gtu.edu.tr");

       MPGroupEmail gtuCse = new MPGroupEmail("Gtu Computer Science", "gtu.edu.tr");
       MPPersonalEmail headOfCse = new MPPersonalEmail("Ibrahim Sogukpinar", "gtu.edu.tr");
       MPPersonalEmail yusufSinanAkgul = new MPPersonalEmail("Yusuf Sinan Akgul", "gtu.edu.tr");
       MPPersonalEmail haciAliMantar = new MPPersonalEmail("Haci Ali Mantar", "gtu.edu.tr");
       MPPersonalEmail erkanZergeroglu = new MPPersonalEmail("Erkan Zergeroglu", "gtu.edu.tr");
       MPPersonalEmail hasariCelebi = new MPPersonalEmail("Hasari Celebi", "gtu.edu.tr");
       MPPersonalEmail mehmetGokturk = new MPPersonalEmail("Mehmet Gokturk", "gtu.edu.tr");
       MPPersonalEmail didemGozupek = new MPPersonalEmail("Didem Gozupek", "gtu.edu.tr");
       MPPersonalEmail fatihErdoganSevilgen = new MPPersonalEmail("Fatih Erdogan Sevilgen", "gtu.edu.tr");
       MPPersonalEmail alpArslanBayrakci = new MPPersonalEmail("Alp Arslan Bayrakci", "gtu.edu.tr");
       MPPersonalEmail yakupGenc = new MPPersonalEmail("Yakup Genc", "gtu.edu.tr");
       MPPersonalEmail urazCengizTurker = new MPPersonalEmail("Uraz Cengiz Turker", "gtu.edu.tr");

       MPGroupEmail gtuElec = new MPGroupEmail("Gtu Electronical Engineering", "gtu.edu.tr");
       MPPersonalEmail headOfElec = new MPPersonalEmail("Hakan Hocaoglu", "gtu.edu.tr");
       MPPersonalEmail serkanAksoy = new MPPersonalEmail("Serkan Aksoy", "gtu.edu.tr");
       MPPersonalEmail aliAlkumru = new MPPersonalEmail("Ali Alkumru", "gtu.edu.tr");
       MPPersonalEmail arifErgin = new MPPersonalEmail("Arif Ergin", "gtu.edu.tr");
       MPPersonalEmail oguzKucur = new MPPersonalEmail("Oguz Kucur", "gtu.edu.tr");
       MPPersonalEmail olegTretyakov = new MPPersonalEmail("Oleg Tretyakov", "gtu.edu.tr");
       MPPersonalEmail yuryTuchkin = new MPPersonalEmail("Yury Tuchkin", "gtu.edu.tr");
       MPPersonalEmail abdulkadirBalikci = new MPPersonalEmail("Abdulkadir Balikci", "gtu.edu.tr");
       MPPersonalEmail fatihDikmen = new MPPersonalEmail("Fatih Dikmen", "gtu.edu.tr");
       MPPersonalEmail korayKayabol = new MPPersonalEmail("Koray Kayabol", "gtu.edu.tr");
       MPPersonalEmail serdarSuerErdem = new MPPersonalEmail("Serdar Suer Erdem", "gtu.edu.tr");
       MPPersonalEmail tubaGozel = new MPPersonalEmail("Tuba Gozel", "gtu.edu.tr");
       MPPersonalEmail koksalHocaoglu = new MPPersonalEmail("Koksal Hocaoglu", "gtu.edu.tr");
       MPPersonalEmail onderSuvak = new MPPersonalEmail("Onder Suvak", "gtu.edu.tr");
       MPPersonalEmail atillaUygur = new MPPersonalEmail("Atilla Uygur", "gtu.edu.tr");
       MPPersonalEmail ardaUlku = new MPPersonalEmail("Arda Ulku", "gtu.edu.tr");

       gtuCse.addEmail(headOfCse);
       gtuCse.addEmail(yusufSinanAkgul);
       gtuCse.addEmail(haciAliMantar);
       gtuCse.addEmail(erkanZergeroglu);
       gtuCse.addEmail(hasariCelebi);
       gtuCse.addEmail(mehmetGokturk);
       gtuCse.addEmail(didemGozupek);
       gtuCse.addEmail(fatihErdoganSevilgen);
       gtuCse.addEmail(alpArslanBayrakci);
       gtuCse.addEmail(yakupGenc);
       gtuCse.addEmail(urazCengizTurker);

       gtuElec.addEmail(headOfElec);
       gtuElec.addEmail(serkanAksoy);
       gtuElec.addEmail(aliAlkumru);
       gtuElec.addEmail(arifErgin);
       gtuElec.addEmail(oguzKucur);
       gtuElec.addEmail(olegTretyakov);
       gtuElec.addEmail(yuryTuchkin);
       gtuElec.addEmail(abdulkadirBalikci);
       gtuElec.addEmail(fatihDikmen);
       gtuElec.addEmail(korayKayabol);
       gtuElec.addEmail(serdarSuerErdem);
       gtuElec.addEmail(tubaGozel);
       gtuElec.addEmail(koksalHocaoglu);
       gtuElec.addEmail(onderSuvak);
       gtuElec.addEmail(atillaUygur);
       gtuElec.addEmail(ardaUlku);

       gtuEngFac.addEmail(gtuCse);
       gtuEngFac.addEmail(gtuElec);

       gtuEngFac.printInfo();
    }
}
