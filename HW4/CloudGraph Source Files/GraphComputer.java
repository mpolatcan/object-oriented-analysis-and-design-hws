import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 * Created by mpolatcan on 25.12.2016.
 */
public interface GraphComputer extends Remote{
    <T> List<Vertex<T>> shortestPathFinderService(String username, Graph<T> graph, T source, T target)
            throws RemoteException,
                   CloudGraphExceptions.VertexNotFoundException,
                   CloudGraphExceptions.UnsufficientCreditsException;
    <T> List<Vertex<T>> hamiltonianCycleFinderService(String username, Graph<T> graph, T source)
            throws RemoteException,
                   CloudGraphExceptions.VertexNotFoundException,
                   CloudGraphExceptions.UnsufficientCreditsException;
    void login(String username, String password) throws RemoteException;
    void logout(String username) throws RemoteException;
    void checkLoginInfo(String username, String password)
            throws RemoteException,
                   CloudGraphExceptions.InvalidLoginCredentialsException;
    void addAccount(String email, String username, String password, Integer creditAmount)
            throws RemoteException;
    void checkNewAccountInfo(String email, String username)
            throws RemoteException,
                   CloudGraphExceptions.UserAlreadyHaveAccountException;
    Integer getCreditAmount(String username) throws RemoteException;
    void buyCredits(String username, Integer creditAmount) throws RemoteException;
}
