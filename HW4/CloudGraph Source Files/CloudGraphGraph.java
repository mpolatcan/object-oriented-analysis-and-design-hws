import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by mpolatcan on 24.12.2016.
 */
public class CloudGraphGraph<T> implements Graph<T>, Serializable{
    private List<Vertex<T>> vertices;
    private List<Edge<T>> edges;

    public CloudGraphGraph() {
        vertices = new ArrayList<>();
        edges = new ArrayList<>();
    }

    @Override
    public Graph<T> addVertex(T val) {
        vertices.add(new CloudGraphVertex<>(val));

        return this;
    }

    @Override
    public Graph<T> removeVertex(T val) throws CloudGraphExceptions.VertexNotFoundException {
        Vertex<T> removedVertex = getVertex(val);

        vertices.remove(removedVertex);
        removeEdgesOfVertex(val);

        return this;
    }

    @Override
    public Graph<T> addEdge(T val, T neighborVal, double edgeVal) throws CloudGraphExceptions.VertexNotFoundException {
        Vertex<T> source = getVertex(val);
        Vertex<T> neighbor = getVertex(neighborVal);

        edges.add(new CloudGraphEdge<>(source, neighbor, edgeVal));

        return this;
    }

    @Override
    public Graph<T> removeEdge(T val, T neighborVal) throws CloudGraphExceptions.EdgeNotFoundException {
        Edge<T> removedEdge = getEdge(val,neighborVal);

        edges.remove(removedEdge);

        return this;
    }

    private void removeEdgesOfVertex(T val) {
        Iterator<Edge<T>> iterEdge = edges.iterator();

        while (iterEdge.hasNext()) {
            Edge<T> edge = iterEdge.next();

            if (edge.getVertex().getVertexValue().equals(val) ||
                edge.getNeighborVertex().getVertexValue().equals(val)) {
                iterEdge.remove();
            }
        }
    }

    public Vertex<T> getVertex(T val) throws CloudGraphExceptions.VertexNotFoundException{
        Iterator<Vertex<T>> iterVertex = vertices.iterator();

        while (iterVertex.hasNext()) {
            Vertex<T> vertex = iterVertex.next();

            if (vertex.getVertexValue().equals(val)) {
                return vertex;
            }
        }

        throw new CloudGraphExceptions.VertexNotFoundException("There is no vertex with value " + val +
                " in graph!");
    }

    public Edge<T> getEdge(T val, T neighborVal) throws CloudGraphExceptions.EdgeNotFoundException{
        Iterator<Edge<T>> iterEdge = edges.iterator();

        while (iterEdge.hasNext()) {
            Edge<T> edge = iterEdge.next();

            if (edge.getVertex().getVertexValue().equals(val) &&
                edge.getNeighborVertex().getVertexValue().equals(neighborVal)) {
                return edge;
            }
        }

        throw new CloudGraphExceptions.EdgeNotFoundException("There is no edge between " + val + " and " + neighborVal
                + " in graph!");
    }

    public List<Vertex<T>> getVertices() {
        return vertices;
    }

    public List<Edge<T>> getEdges() { return edges; }

    public int getVertexSize() {
        return vertices.size();
    }

}
