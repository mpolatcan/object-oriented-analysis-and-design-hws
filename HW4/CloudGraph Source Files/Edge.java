/**
 * Created by mpolatcan on 24.12.2016.
 */
public interface Edge<T> {
    void setVertex(Vertex<T> x);
    void setNeighborVertex(Vertex<T> x);
    double getEdgeValue();
    void setEdgeVal(double val);
    Vertex<T> getVertex();
    Vertex<T> getNeighborVertex();
}
