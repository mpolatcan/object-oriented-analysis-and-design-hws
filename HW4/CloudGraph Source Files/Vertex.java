/**
 * Created by mpolatcan on 24.12.2016.
 */
public interface Vertex<T> {
    void setVertexValue(T val);
    T getVertexValue();
}
