import java.util.*;

/**
 * Created by mpolatcan on 24.12.2016.
 */
public class CloudGraphShortestPathFinder<T> {
    private Graph<T> graph;
    private final List<Edge<T>> edges;
    private final List<Vertex<T>> vertices;
    private Set<Vertex<T>> settledNodes;
    private Set<Vertex<T>> unsettledNodes;
    private Map<Vertex<T>, Vertex<T>> predecessors;
    private Map<Vertex<T>, Double> distance;

    public CloudGraphShortestPathFinder(Graph<T> graph) {
        this.graph = graph;
        edges = new ArrayList<>(graph.getEdges());
        vertices = new ArrayList<>(graph.getVertices());
    }

    public LinkedList<Vertex<T>> findShortestPath(T sourceVal, T targetVal)
            throws CloudGraphExceptions.VertexNotFoundException {
        Vertex<T> source = graph.getVertex(sourceVal);
        Vertex<T> target = graph.getVertex(targetVal);

        prepareAlgorithm(source);

        LinkedList<Vertex<T>> shortestPath = new LinkedList<>();
        Vertex<T> step = target;

        if (predecessors.get(step) == null) {
            return null;
        }

        shortestPath.add(step);

        while (predecessors.get(step) != null) {
            step = predecessors.get(step);
            shortestPath.add(step);
        }

        Collections.reverse(shortestPath);
        return shortestPath;
    }

    private void prepareAlgorithm(Vertex<T> source) {
       settledNodes = new HashSet<>();
       unsettledNodes = new HashSet<>();
       distance = new HashMap<>();
       predecessors = new HashMap<>();

       distance.put(source,0.0);
       unsettledNodes.add(source);

       while (unsettledNodes.size() > 0) {
           Vertex<T> node = getMinimum(unsettledNodes);
           settledNodes.add(node);
           unsettledNodes.remove(node);
           findMinimalDistances(node);
       }
    }

    private void findMinimalDistances(Vertex<T> node) {
        List<Vertex<T>> adjacentNodes = getNeighbors(node);

        for (Vertex<T> target : adjacentNodes) {
            if (getShortestDistance(target) > getShortestDistance(node) + getDistance(node,target)) {
                distance.put(target,getShortestDistance(node) + getDistance(node,target));
                predecessors.put(target,node);
                unsettledNodes.add(target);
            }
        }
    }

    private double getDistance(Vertex<T> node, Vertex target) {
        for (Edge<T> edge : edges) {
            if (edge.getVertex().equals(node) &&
                edge.getNeighborVertex().equals(target)) {
                return edge.getEdgeValue();
            }
        }

        throw new RuntimeException("Should not happen");
    }

    private List<Vertex<T>> getNeighbors(Vertex<T> node) {
        List<Vertex<T>> neighbors = new ArrayList<>();

        for (Edge<T> edge : edges) {
            if (edge.getVertex().equals(node) &&
                !isSettled(edge.getNeighborVertex())) {
                neighbors.add(edge.getNeighborVertex());
            }
        }

        return neighbors;
    }

    private Vertex<T> getMinimum(Set<Vertex<T>> vertexes) {
        Vertex<T> minimum = null;

        for (Vertex<T> vertex : vertexes) {
            if (minimum == null) {
                minimum = vertex;
            } else {
                if (getShortestDistance(vertex) < getShortestDistance(minimum)) {
                    minimum = vertex;
                }
            }
        }

        return minimum;
    }

    private boolean isSettled(Vertex<T> vertex) {
        return settledNodes.contains(vertex);
    }

    private double getShortestDistance(Vertex<T> destination) {
        Double d = distance.get(destination);

        if (d == null) {
            return Double.MAX_VALUE;
        } else {
            return d;
        }
    }

    public void setGraph(Graph<T> graph) {
        this.graph = graph;
    }
}
