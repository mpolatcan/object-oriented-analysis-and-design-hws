import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by mpolatcan on 30.12.2016.
 */
public class CloudGraphClientPanel extends Application {
    private Graph clientGraph;
    private GraphComputer graphComputer;
    private String username;
    private String graphDataType;
    private String operationType;
    /* -------------------- UI Components ---------------- */
    private Parent root;
    private Button registerButton;
    private Button registerPaneBackButton;
    private Button registerPaneOkButton;
    private Button loginButton;
    private Button loginPaneBackButton;
    private Button loginPaneOkButton;
    private Button vertexAdderAddButton;
    private Button edgeAdderAddButton;
    private Button doOperationButton;
    private Button logoutButton;
    private Button buyCreditsButton;
    private Button exitButton;
    private Button connectButton;
    private TextField loginPaneUsernameTextField;
    private PasswordField loginPanePasswordTextField;
    private TextField registerPaneEmailTextField;
    private TextField registerPaneUsernameTextField;
    private PasswordField registerPanePasswordTextField;
    private TextField vertexAdderValueTextField;
    private TextField edgeAdderWeightTextField;
    private TextField creditAmountTextField;
    private TextField serverIpTextField;
    private TextField serverPortTextField;
    private Pane loginPane;
    private Pane registerPane;
    private Pane connectionErrorPane;
    private Pane paneOverlay;
    private Pane mainMenuPane;
    private Pane operationPane;
    private Pane serverConnectPane;
    private ImageView loginPaneUsernameCorrectIcon;
    private ImageView loginPaneUsernameInvalidIcon;
    private ImageView loginPanePasswordCorrectIcon;
    private ImageView loginPanePasswordInvalidIcon;
    private ImageView registerPaneEmailCorrectIcon;
    private ImageView registerPaneEmailInvalidIcon;
    private ImageView registerPaneUsernameCorrectIcon;
    private ImageView registerPaneUsernameInvalidIcon;
    private ComboBox graphDataTypeComboBox;
    private ComboBox edgeAdderSourceComboBox;
    private ComboBox edgeAdderTargetComboBox;
    private ComboBox operationTypeComboBox;
    private ComboBox sourceVertexParamComboBox;
    private ComboBox targetVertexParamComboBox;
    private ListView vertexList;
    private ListView edgeList;
    private TextArea resultsLog;
    private TextArea userInfo;
    private Label clock;
    /* ---------------------------------------------------- */

    @Override
    public void start(Stage stage) throws Exception {
        root = FXMLLoader.load(getClass().getResource("cloudGraphClientUI.fxml"));

        stage.setTitle("Cloud Graph Client Panel");
        stage.setScene(new Scene(root, 800,600));
        stage.setResizable(false);
        stage.show();

        prepareUIComponents();
        prepareConnectPane();
        prepareLoginPaneAndLoginButton();
        prepareRegisterPaneAndRegisterButton();
        preparegraphDataTypeComboBox();
        prepareOperationTypeComboBoxAndDoButton();
        prepareVertexAdder();
        prepareEdgeAdder();
        prepareBuyCredits();
        prepareLogout();
        prepareExit();
        startClock();
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        System.exit(0);
    }

    private void connectToServer(String ip, int port) throws RemoteException, NotBoundException {
        String hostName = CloudGraphConstants.BULUT_CIZGE_HOSTNAME;
        Registry registry = LocateRegistry.getRegistry(ip, port);
        graphComputer = ((GraphComputer) registry.lookup(hostName));
    }

    private void prepareUIComponents() {
        connectionErrorPane = ((Pane) root.lookup("#cloudGraphConnectionErrorPane"));
        mainMenuPane = ((Pane) root.lookup("#cloudGraphMainMenuPane"));
        operationPane = ((Pane) root.lookup("#cloudGraphOperationPane"));
        serverConnectPane = ((Pane) root.lookup("#cloudGraphServerConnectPane"));
        paneOverlay = ((Pane) root.lookup("#cloudGraphPaneOverlay"));
        graphDataTypeComboBox = ((ComboBox) root.lookup("#cloudGraphGraphTypeComboBox"));
        operationTypeComboBox = ((ComboBox) root.lookup("#cloudGraphOperationTypeComboBox"));
        sourceVertexParamComboBox = ((ComboBox) root.lookup("#cloudGraphSourceVertexParamComboBox"));
        targetVertexParamComboBox = ((ComboBox) root.lookup("#cloudGraphTargetVertexParamComboBox"));
        doOperationButton = ((Button) root.lookup("#cloudGraphDoOperationButton"));
        buyCreditsButton = ((Button) root.lookup("#cloudGraphBuyCreditsButton"));
        logoutButton = ((Button) root.lookup("#cloudGraphLogoutButton"));
        exitButton = ((Button) root.lookup("#cloudGraphExitButton"));
        connectButton = ((Button) root.lookup("#cloudGraphConnectButton"));
        creditAmountTextField = ((TextField) root.lookup("#cloudGraphCreditAmountTextField"));
        serverIpTextField = ((TextField) root.lookup("#cloudGraphServerIpTextField"));
        serverPortTextField = ((TextField) root.lookup("#cloudGraphServerPortTextField"));

        clock = ((Label) root.lookup("#cloudGraphClock"));
        clock.setStyle("-fx-background-color: black; -fx-border-color: white; -fx-border-width: 2px");

        userInfo = ((TextArea) root.lookup("#cloudGraphUserInfo"));
        userInfo.setStyle("-fx-text-fill: #00B4F4; -fx-border-color: white; -fx-border-width: 2px");
        Region regionUserInfo = ((Region) userInfo.lookup(".content"));
        regionUserInfo.setStyle("-fx-background-color: black");

        resultsLog = ((TextArea) root.lookup("#cloudGraphResultsLog"));
        resultsLog.setStyle("-fx-text-fill: #00B4F4; -fx-border-color: white; -fx-border-width: 2px");
        Region region = ((Region) resultsLog.lookup(".content"));
        region.setStyle("-fx-background-color: black");

        vertexAdderValueTextField = ((TextField) root.lookup("#cloudGraphVertexAdderValueTextField"));
        vertexAdderAddButton = ((Button) root.lookup("#cloudGraphVertexAdderAddButton"));
        vertexList = ((ListView) root.lookup("#cloudGraphVertexList"));

        edgeAdderWeightTextField = ((TextField) root.lookup("#cloudGraphEdgeAdderWeightTextField"));
        edgeAdderAddButton = ((Button) root.lookup("#cloudGraphEdgeAdderAddButton"));
        edgeAdderSourceComboBox = ((ComboBox) root.lookup("#cloudGraphEdgeAdderSourceComboBox"));
        edgeAdderTargetComboBox = ((ComboBox) root.lookup("#cloudGraphEdgeAdderTargetComboBox"));
        edgeList = ((ListView) root.lookup("#cloudGraphEdgeList"));

        loginButton = ((Button) root.lookup("#cloudGraphLoginButton"));
        loginPane = ((Pane) root.lookup("#cloudGraphLoginPane"));
        loginPaneBackButton = ((Button) loginPane.lookup("#cloudGraphLoginPaneBackButton"));
        loginPaneOkButton = ((Button) loginPane.lookup("#cloudGraphLoginPaneOkButton"));
        loginPaneUsernameTextField = ((TextField) loginPane.lookup("#cloudGraphLoginPaneUsernameTextField"));
        loginPanePasswordTextField = ((PasswordField) loginPane.lookup("#cloudGraphLoginPanePasswordTextField"));
        loginPaneUsernameCorrectIcon = ((ImageView) loginPane.lookup("#cloudGraphLoginPaneUsernameCorrectIcon"));
        loginPaneUsernameInvalidIcon = ((ImageView) loginPane.lookup("#cloudGraphLoginPaneUsernameInvalidIcon"));
        loginPanePasswordCorrectIcon = ((ImageView) loginPane.lookup("#cloudGraphLoginPanePasswordCorrectIcon"));
        loginPanePasswordInvalidIcon = ((ImageView) loginPane.lookup("#cloudGraphLoginPanePasswordInvalidIcon"));

        registerButton = ((Button) root.lookup("#cloudGraphRegisterButton"));
        registerPane = ((Pane) root.lookup("#cloudGraphRegisterPane"));
        registerPaneBackButton = ((Button) registerPane.lookup("#cloudGraphRegisterPaneBackButton"));
        registerPaneOkButton = ((Button) registerPane.lookup("#cloudGraphRegisterPaneOkButton"));
        registerPaneEmailTextField = ((TextField) registerPane.lookup("#cloudGraphRegisterPaneEmailTextField"));
        registerPaneUsernameTextField = ((TextField) registerPane.lookup("#cloudGraphRegisterPaneUsernameTextField"));
        registerPanePasswordTextField = ((PasswordField) registerPane.lookup("#cloudGraphRegisterPanePasswordTextField"));
        registerPaneEmailCorrectIcon = ((ImageView) registerPane.lookup("#cloudGraphRegisterPaneEmailCorrectIcon"));
        registerPaneEmailInvalidIcon = ((ImageView) registerPane.lookup("#cloudGraphRegisterPaneEmailInvalidIcon"));
        registerPaneUsernameCorrectIcon = ((ImageView) registerPane.lookup("#cloudGraphRegisterPaneUsernameCorrectIcon"));
        registerPaneUsernameInvalidIcon = ((ImageView) registerPane.lookup("#cloudGraphRegisterPaneUsernameInvalidIcon"));
    }

    private void prepareConnectPane() {
        serverPortTextField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String s, String t1) {
                if (detectInteger(serverPortTextField.getText()) && checkIPv4(serverIpTextField.getText())) {
                    connectButton.setDisable(false);
                } else {
                    connectButton.setDisable(true);
                }
            }
        });

        serverIpTextField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String s, String t1) {
                if (detectInteger(serverPortTextField.getText()) && checkIPv4(serverIpTextField.getText())) {
                    connectButton.setDisable(false);
                } else {
                    connectButton.setDisable(true);
                }
            }
        });

        connectButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                try {
                    connectToServer(serverIpTextField.getText(),Integer.parseInt(serverPortTextField.getText()));
                    mainMenuPane.setVisible(true);
                    serverConnectPane.setVisible(false);
                } catch (RemoteException | NotBoundException ex) {
                    serverConnectPane.setVisible(false);
                    connectionErrorPane.setVisible(true);
                    paneOverlay.setVisible(true);
                }
            }
        });
    }

    private final boolean checkIPv4(final String ip) {
        boolean isIPv4;
        try {
            final InetAddress inet = InetAddress.getByName(ip);
            isIPv4 = inet.getHostAddress().equals(ip)
                    && inet instanceof Inet4Address;
        } catch (final UnknownHostException e) {
            isIPv4 = false;
        }
        return isIPv4;
    }

    private void prepareExit() {
        exitButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                try {
                    stop();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    private void prepareBuyCredits() {
        creditAmountTextField.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (keyEvent.getCode().equals(KeyCode.ENTER)) {
                    if (!buyCreditsButton.isDisable()) {
                        buyCreditsHelper();
                    }
                }
            }
        });

        creditAmountTextField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String s, String t1) {
                if (detectInteger(t1)) {
                    buyCreditsButton.setDisable(false);
                } else {
                    buyCreditsButton.setDisable(true);
                }
            }
        });

        buyCreditsButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
               buyCreditsHelper();
            }
        });
    }

    private void buyCreditsHelper() {
        try {
            graphComputer.buyCredits(username,Integer.parseInt(creditAmountTextField.getText()));
            userInfo.appendText("\n" + creditAmountTextField.getText() + " credits loaded!");
            creditAmountTextField.clear();
        } catch (RemoteException ex) {
            ex.printStackTrace();
        }
    }

    private void prepareLogout() {
        logoutButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                try {
                    graphComputer.logout(username);
                    operationPane.setVisible(false);
                    mainMenuPane.setVisible(true);
                } catch (RemoteException ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    private void preparegraphDataTypeComboBox() {
        ArrayList<String> graphDataTypesList = new ArrayList<>();
        graphDataTypesList.add(CloudGraphConstants.GRAPH_DATA_TYPE_INTEGER);
        graphDataTypesList.add(CloudGraphConstants.GRAPH_DATA_TYPE_DOUBLE);
        graphDataTypesList.add(CloudGraphConstants.GRAPH_DATA_TYPE_FLOAT);
        graphDataTypesList.add(CloudGraphConstants.GRAPH_DATA_TYPE_STRING);
        
        graphDataTypeComboBox.getItems().addAll(graphDataTypesList);
        graphDataTypeComboBox.getSelectionModel().select(0);

        // Initial values for graph and graph type
        clientGraph = new CloudGraphGraph<Integer>();
        graphDataType = CloudGraphConstants.GRAPH_DATA_TYPE_INTEGER;

        graphDataTypeComboBox.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String s, String t1) {
                if (t1.equals(CloudGraphConstants.GRAPH_DATA_TYPE_INTEGER)) {
                    clientGraph = new CloudGraphGraph<Integer>();
                    graphDataType = CloudGraphConstants.GRAPH_DATA_TYPE_INTEGER;
                } else if (t1.equals(CloudGraphConstants.GRAPH_DATA_TYPE_DOUBLE)) {
                    clientGraph = new CloudGraphGraph<Double>();
                    graphDataType = CloudGraphConstants.GRAPH_DATA_TYPE_DOUBLE;
                } else if (t1.equals(CloudGraphConstants.GRAPH_DATA_TYPE_FLOAT)) {
                    clientGraph = new CloudGraphGraph<Float>();
                    graphDataType = CloudGraphConstants.GRAPH_DATA_TYPE_FLOAT;
                } else if (t1.equals(CloudGraphConstants.GRAPH_DATA_TYPE_STRING)) {
                    clientGraph = new CloudGraphGraph<String>();
                    graphDataType = CloudGraphConstants.GRAPH_DATA_TYPE_STRING;
                }

                vertexAdderValueTextField.clear();
                edgeAdderWeightTextField.clear();
                edgeAdderSourceComboBox.getItems().clear();
                edgeAdderTargetComboBox.getItems().clear();
                sourceVertexParamComboBox.getItems().clear();
                targetVertexParamComboBox.getItems().clear();
                edgeList.getItems().clear();
                vertexList.getItems().clear();
            }
        });
    }

    private void prepareOperationTypeComboBoxAndDoButton() {
        ArrayList<String> operationTypes = new ArrayList<>();
        operationTypes.add(CloudGraphConstants.SHORTEST_PATH);
        operationTypes.add(CloudGraphConstants.HAMILTONIAN_CYCLE);

        operationTypeComboBox.getItems().addAll(operationTypes);
        operationTypeComboBox.getSelectionModel().select(0);
        operationType = CloudGraphConstants.SHORTEST_PATH;

        operationTypeComboBox.valueProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observableValue, Object o, Object t1) {
                if (t1.equals(CloudGraphConstants.SHORTEST_PATH)) {
                    targetVertexParamComboBox.setVisible(true);
                    operationType = CloudGraphConstants.SHORTEST_PATH;
                } else if (t1.equals(CloudGraphConstants.HAMILTONIAN_CYCLE)) {
                    targetVertexParamComboBox.setVisible(false);
                    operationType = CloudGraphConstants.HAMILTONIAN_CYCLE;
                }
            }
        });

        doOperationButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                List<Vertex> result;
                if (operationType.equals(CloudGraphConstants.SHORTEST_PATH)) {
                    try {
                         result = graphComputer.shortestPathFinderService(
                                username,
                                clientGraph,
                                dataConverter(sourceVertexParamComboBox.getSelectionModel().getSelectedItem().toString()),
                                dataConverter(targetVertexParamComboBox.getSelectionModel().getSelectedItem().toString()));
                        resultsLog.appendText("Result: " + result + "\n");
                    } catch (CloudGraphExceptions.UnsufficientCreditsException | RemoteException |
                            CloudGraphExceptions.VertexNotFoundException ex) {
                        resultsLog.appendText(ex.getMessage() + "\n");
                    }
                } else if (operationType.equals(CloudGraphConstants.HAMILTONIAN_CYCLE)) {
                    try {
                        result = graphComputer.hamiltonianCycleFinderService(
                                username,
                                clientGraph,
                                dataConverter(sourceVertexParamComboBox.getSelectionModel().getSelectedItem().toString()));
                        resultsLog.appendText("Result: " + result + "\n");
                    } catch (CloudGraphExceptions.UnsufficientCreditsException | RemoteException |
                            CloudGraphExceptions.VertexNotFoundException ex) {
                        resultsLog.appendText(ex.getMessage() + "\n");
                    }
                }
            }
        });

        sourceVertexParamComboBox.valueProperty().addListener(paramsChangedListener);
        targetVertexParamComboBox.valueProperty().addListener(paramsChangedListener);
    }

    private void prepareVertexAdder() {
        vertexAdderValueTextField.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (keyEvent.getCode().equals(KeyCode.ENTER)) {
                    if (!vertexAdderAddButton.isDisable()) {
                        addVertexHelper();
                    }
                }
            }
        });

        vertexAdderValueTextField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String s, String t1) {
                if (graphInputTypeChecker(vertexAdderValueTextField.getText()) &&
                    !vertexAdderValueTextField.getText().equals("")) {
                    vertexAdderAddButton.setDisable(false);
                } else {
                    vertexAdderAddButton.setDisable(true);
                }
            }
        });

        vertexAdderAddButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                addVertexHelper();
            }
        });
    }

    private void addVertexHelper() {
        clientGraph.addVertex(dataConverter(vertexAdderValueTextField.getText()));

        edgeAdderSourceComboBox.getItems().clear();
        edgeAdderTargetComboBox.getItems().clear();
        sourceVertexParamComboBox.getItems().clear();
        targetVertexParamComboBox.getItems().clear();

        edgeAdderSourceComboBox.getItems().addAll(clientGraph.getVertices());
        edgeAdderTargetComboBox.getItems().addAll(clientGraph.getVertices());
        sourceVertexParamComboBox.getItems().addAll(clientGraph.getVertices());
        targetVertexParamComboBox.getItems().addAll(clientGraph.getVertices());

        vertexAdderValueTextField.clear();

        ObservableList<String> vertices = FXCollections.observableArrayList(clientGraph.getVertices());
        vertexList.setItems(vertices);
    }

    private void prepareEdgeAdder() {
        edgeAdderWeightTextField.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (keyEvent.getCode().equals(KeyCode.ENTER)) {
                    if (!edgeAdderAddButton.isDisable()) {
                        addEdgeHelper();
                    }
                }
            }
        });

        edgeAdderAddButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                addEdgeHelper();
            }
        });

        edgeAdderWeightTextField.textProperty().addListener(edgeAdderChangedListener);

        edgeAdderSourceComboBox.valueProperty().addListener(edgeAdderChangedListener);

        edgeAdderTargetComboBox.valueProperty().addListener(edgeAdderChangedListener);
    }

    private void addEdgeHelper() {
        try {
            clientGraph.addEdge(
                    dataConverter(edgeAdderSourceComboBox.getSelectionModel().getSelectedItem().toString()),
                    dataConverter(edgeAdderTargetComboBox.getSelectionModel().getSelectedItem().toString()),
                    Double.parseDouble(edgeAdderWeightTextField.getText()));
            edgeAdderWeightTextField.clear();
        } catch (CloudGraphExceptions.VertexNotFoundException e) {
            e.printStackTrace();
        }

        ObservableList<String> edges = FXCollections.observableArrayList(clientGraph.getEdges());
        edgeList.setItems(edges);
    }

    private void prepareLoginPaneAndLoginButton() {
        loginButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                loginPane.setVisible(true);
                paneOverlay.setVisible(true);
            }
        });

        loginPaneBackButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                loginPane.setVisible(false);
                paneOverlay.setVisible(false);
                clearTextFields();
            }
        });

        loginPaneOkButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                try {
                    graphComputer.login(loginPaneUsernameTextField.getText(),loginPanePasswordTextField.getText());
                    username = loginPaneUsernameTextField.getText();
                    loginPane.setVisible(false);
                    paneOverlay.setVisible(false);
                    mainMenuPane.setVisible(false);
                    operationPane.setVisible(true);
                    clearTextFields();
                    startUserInfoUpdater();
                } catch (RemoteException ex) {
                    ex.printStackTrace();
                }
            }
        });

        loginPaneUsernameTextField.textProperty().addListener(loginTextChangedListener);
        loginPanePasswordTextField.textProperty().addListener(loginTextChangedListener);
    }

    private void startUserInfoUpdater() {
        ScheduledExecutorService userInfoUpdater = Executors.newSingleThreadScheduledExecutor();

        userInfoUpdater.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                userInfo.clear();
                try {
                    userInfo.appendText("Welcome " + username + "\n");
                    userInfo.appendText("Credits Amount: " + graphComputer.getCreditAmount(username));
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        },0,1,TimeUnit.SECONDS);
    }

    private void clearTextFields() {
        loginPaneUsernameTextField.clear();
        loginPanePasswordTextField.clear();
        registerPaneEmailTextField.clear();
        registerPaneUsernameTextField.clear();
        registerPanePasswordTextField.clear();
    }

    private void prepareRegisterPaneAndRegisterButton() {
        registerButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                registerPane.setVisible(true);
                paneOverlay.setVisible(true);
            }
        });

        registerPaneBackButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                registerPane.setVisible(false);
                paneOverlay.setVisible(false);
                clearTextFields();
            }
        });

        registerPaneOkButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                try {
                    graphComputer.addAccount(registerPaneEmailTextField.getText(),
                                             registerPaneUsernameTextField.getText(),
                                             registerPanePasswordTextField.getText(),
                                             100);
                    username = registerPaneUsernameTextField.getText();
                    registerPane.setVisible(false);
                    paneOverlay.setVisible(false);
                    mainMenuPane.setVisible(false);
                    operationPane.setVisible(true);
                    clearTextFields();
                    startUserInfoUpdater();
                } catch (RemoteException ex) {
                    ex.printStackTrace();
                }
            }
        });

        registerPaneEmailTextField.textProperty().addListener(registerTextChangedListener);
        registerPaneUsernameTextField.textProperty().addListener(registerTextChangedListener);
        registerPanePasswordTextField.textProperty().addListener(registerTextChangedListener);
    }

    private void startClock() {
        ScheduledExecutorService clockService = Executors.newSingleThreadScheduledExecutor();
        clockService.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        LocalTime now = LocalTime.now();
                        clock.setText(" " + convertClockStr(now.getHour() + 1,true) + ":" +
                                convertClockStr(now.getMinute(),false)+ ":" +
                                convertClockStr(now.getSecond(),false));
                    }
                });
            }
        }, 0, 500, TimeUnit.MILLISECONDS);
    }

    private String convertClockStr(int value, boolean hour) {
        if (!hour) {
            return value < 10 ? "0" + value : "" + value;
        } else {
            if (value < 10)
                return "0" + value;
            else if (value == 24)
                return "00";
            else
                return "" + value;
        }
    }

    private boolean graphInputTypeChecker(String clientInput) {
        if (graphDataType == CloudGraphConstants.GRAPH_DATA_TYPE_INTEGER) {
            return detectInteger(clientInput);
        } else if (graphDataType == CloudGraphConstants.GRAPH_DATA_TYPE_DOUBLE) {
            return detectDouble(clientInput);
        } else if (graphDataType == CloudGraphConstants.GRAPH_DATA_TYPE_FLOAT) {
            return detectFloat(clientInput);
        } else if (graphDataType == CloudGraphConstants.GRAPH_DATA_TYPE_STRING) {
            return true;
        } else {
            return false;
        }
    }

    private boolean detectInteger(String integerVal) {
        try {
            Integer.parseInt(integerVal);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    private boolean detectDouble(String doubleVal) {
        try {
            Double.parseDouble(doubleVal);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    private boolean detectFloat(String floatVal) {
        try {
            Float.parseFloat(floatVal);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    private Object dataConverter(String clientInput) {
        try {
            if (graphDataType == CloudGraphConstants.GRAPH_DATA_TYPE_INTEGER) {
                return Integer.parseInt(clientInput);
            } else if (graphDataType == CloudGraphConstants.GRAPH_DATA_TYPE_DOUBLE) {
                return Double.parseDouble(clientInput);
            } else if (graphDataType == CloudGraphConstants.GRAPH_DATA_TYPE_FLOAT) {
                return Float.parseFloat(clientInput);
            } else if (graphDataType == CloudGraphConstants.GRAPH_DATA_TYPE_STRING) {
                return clientInput;
            }
        } catch (InputMismatchException ex) {
            return null;
        }

        return null;
    }

    private ChangeListener<String> loginTextChangedListener = new ChangeListener<String>() {
        @Override
        public void changed(ObservableValue<? extends String> observableValue, String s, String t1) {
            try {
                graphComputer.checkLoginInfo(loginPaneUsernameTextField.getText(),
                        loginPanePasswordTextField.getText());
                 loginPaneUsernameCorrectIcon.setVisible(true);
                 loginPanePasswordCorrectIcon.setVisible(true);
                 loginPanePasswordInvalidIcon.setVisible(false);
                 loginPaneUsernameInvalidIcon.setVisible(false);
                loginPaneOkButton.setDisable(false);
            } catch (RemoteException | CloudGraphExceptions.InvalidLoginCredentialsException ex) {
                loginPaneOkButton.setDisable(true);

                if (ex.getMessage().equals("Username invalid") &&
                    !loginPaneUsernameTextField.getText().equals("")) {
                        loginPaneUsernameInvalidIcon.setVisible(true);
                        loginPaneUsernameCorrectIcon.setVisible(false);
                        loginPanePasswordCorrectIcon.setVisible(false);
                        loginPanePasswordInvalidIcon.setVisible(false);
                } else if (!loginPaneUsernameTextField.getText().equals("")){
                        loginPaneUsernameCorrectIcon.setVisible(true);
                        loginPaneUsernameInvalidIcon.setVisible(false);
                } else {
                    loginPaneUsernameCorrectIcon.setVisible(false);
                    loginPaneUsernameInvalidIcon.setVisible(false);
                }

                if (ex.getMessage().equals("Password invalid") &&
                    !loginPanePasswordTextField.getText().equals("")) {
                        loginPanePasswordInvalidIcon.setVisible(true);
                } else {
                    loginPanePasswordCorrectIcon.setVisible(false);
                    loginPanePasswordInvalidIcon.setVisible(false);
                }
            }
        }
    };

    private ChangeListener<String> registerTextChangedListener = new ChangeListener<String>() {
        @Override
        public void changed(ObservableValue<? extends String> observableValue, String s, String t1) {
            try {
                graphComputer.checkNewAccountInfo(registerPaneEmailTextField.getText(),
                                                  registerPaneUsernameTextField.getText());
                registerPaneEmailCorrectIcon.setVisible(true);
                registerPaneUsernameCorrectIcon.setVisible(true);
                registerPaneEmailInvalidIcon.setVisible(false);
                registerPaneUsernameInvalidIcon.setVisible(false);

                if (registerPaneEmailTextField.getText().equals("")) {
                    registerPaneEmailInvalidIcon.setVisible(false);
                    registerPaneEmailCorrectIcon.setVisible(false);
                }

                if (registerPaneUsernameTextField.getText().equals("")) {
                    registerPaneUsernameCorrectIcon.setVisible(false);
                    registerPaneUsernameInvalidIcon.setVisible(false);
                }

                if (!registerPanePasswordTextField.getText().equals("") &&
                    !registerPaneUsernameTextField.getText().equals("") &&
                    !registerPaneEmailTextField.getText().equals(""))
                    registerPaneOkButton.setDisable(false);
            } catch (RemoteException |
                    CloudGraphExceptions.UserAlreadyHaveAccountException ex) {
                registerPaneOkButton.setDisable(true);

                if (ex.getMessage().equals("Username and email are invalid")) {
                    registerPaneEmailInvalidIcon.setVisible(true);
                    registerPaneUsernameInvalidIcon.setVisible(true);
                    registerPaneEmailCorrectIcon.setVisible(false);
                    registerPaneUsernameCorrectIcon.setVisible(false);
                } else if (ex.getMessage().equals("Username invalid")) {
                    registerPaneUsernameInvalidIcon.setVisible(true);
                    registerPaneUsernameCorrectIcon.setVisible(false);

                    if (!registerPaneEmailTextField.getText().equals("")) {
                        registerPaneEmailInvalidIcon.setVisible(false);
                        registerPaneEmailCorrectIcon.setVisible(true);
                    } else {
                        registerPaneEmailCorrectIcon.setVisible(false);
                        registerPaneEmailInvalidIcon.setVisible(false);
                    }
                } else if (ex.getMessage().equals("Email invalid")) {
                    registerPaneEmailInvalidIcon.setVisible(true);
                    registerPaneEmailCorrectIcon.setVisible(false);

                    if (!registerPaneUsernameTextField.getText().equals("")) {
                        registerPaneUsernameInvalidIcon.setVisible(false);
                        registerPaneUsernameCorrectIcon.setVisible(true);
                    } else {
                        registerPaneUsernameCorrectIcon.setVisible(false);
                        registerPaneUsernameInvalidIcon.setVisible(false);
                    }
                }
            }
        }
    };

    private ChangeListener edgeAdderChangedListener = new ChangeListener() {
        @Override
        public void changed(ObservableValue observableValue, Object o, Object t1) {
            if (edgeAdderSourceComboBox.getSelectionModel().getSelectedItem() != null &&
                edgeAdderTargetComboBox.getSelectionModel().getSelectedItem() != null &&
                detectDouble(edgeAdderWeightTextField.getText())) {
                edgeAdderAddButton.setDisable(false);
            } else {
                edgeAdderAddButton.setDisable(true);
            }
        }
    };

    private ChangeListener paramsChangedListener = new ChangeListener() {
        @Override
        public void changed(ObservableValue observableValue, Object o, Object t1) {
            if (operationType.equals(CloudGraphConstants.SHORTEST_PATH)) {
                if (sourceVertexParamComboBox.getSelectionModel().getSelectedItem() != null &&
                    targetVertexParamComboBox.getSelectionModel().getSelectedItem() != null) {
                    doOperationButton.setDisable(false);
                } else {
                    doOperationButton.setDisable(true);
                }
            } else if (operationType.equals(CloudGraphConstants.HAMILTONIAN_CYCLE)) {
                if (sourceVertexParamComboBox.getSelectionModel().getSelectedItem() != null) {
                    doOperationButton.setDisable(false);
                } else {
                    doOperationButton.setDisable(true);
                }
            }
        }
    };

    public static void main(String[] args) {
        launch(args);
    }
}
