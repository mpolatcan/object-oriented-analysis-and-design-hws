import java.io.Serializable;

/**
 * Created by mpolatcan on 26.12.2016.
 */
public class CloudGraphUser implements Serializable{
    private String email;
    private String username;
    private String password;
    private Integer creditAmount;
    private static final long serialVersionUID = -1129698854291510207L;

    public CloudGraphUser() {

    }

    public CloudGraphUser(String email, String username, String password, Integer creditAmount) {
        this.email = email;
        this.username = username;
        this.password = password;
        this.creditAmount = creditAmount;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setCreditAmount(Integer creditAmount) {
        this.creditAmount = creditAmount;
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public Integer getCreditAmount() {
        return creditAmount;
    }

    @Override
    public String toString() {
        return String.format("%s", username);
    }
}