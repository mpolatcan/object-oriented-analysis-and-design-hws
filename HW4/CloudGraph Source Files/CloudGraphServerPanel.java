import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

import java.net.*;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class CloudGraphServerPanel extends Application {
    private CloudGraphServer cloudGraphServer;
    private Parent root;
    private TextArea cloudGraphLogArea;
    private TextArea cloudGraphServerInfo;
    private ListView<String> cloudGraphOnlineUsersList;
    private LineChart<Number, Number> cloudGraphServerTrafficGraph;
    private Label clock;
    private Button startServerButton;
    private TextField portTextField;
    private ComboBox ipComboBox;
    private int serverTrafficTime = 0;
    private Pane serverConfigPane;
    private Pane serverMainPane;

    @Override
    public void start(Stage stage) throws Exception{
        root = FXMLLoader.load(getClass().getResource("cloudGraphServerUI.fxml"));
        stage.setTitle("Cloud Graph Server Panel");
        stage.setResizable(false);
        stage.setScene(new Scene(root, 800, 600));
        stage.show();

        prepareServerConfigPanel();
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        System.exit(0);
    }

    private void prepareServerConfigPanel() {
        serverConfigPane = ((Pane) root.lookup("#cloudGraphServerConfigPane"));
        serverMainPane = ((Pane) root.lookup("#cloudGraphServerPanelMainPane"));
        startServerButton = ((Button) root.lookup("#cloudGraphStartServerButton"));
        ipComboBox = ((ComboBox) root.lookup("#cloudGraphServerIpComboBox"));
        portTextField = ((TextField) root.lookup("#cloudGraphServerPortTextField"));

        startServerButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                 Integer.parseInt(portTextField.getText());
                try {
                    prepareLogArea();
                    prepareServer(ipComboBox.getSelectionModel().getSelectedItem().toString(),
                                  Integer.parseInt(portTextField.getText()));
                    prepareServerTrafficGraph();
                    prepareOnlineUsersList();
                    startClock();

                    serverConfigPane.setVisible(false);
                    serverMainPane.setVisible(true);
                } catch (RemoteException e) {
                    e.printStackTrace();
                } catch (NotBoundException e) {
                    e.printStackTrace();
                }
            }
        });

        ArrayList<String> serverIps = new ArrayList<>();
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();

            while (networkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = networkInterfaces.nextElement();
                Enumeration<InetAddress> inetAddress = networkInterface.getInetAddresses();
                while (inetAddress.hasMoreElements()) {
                    InetAddress address = inetAddress.nextElement();
                    if (checkIPv4(address.getHostAddress())) {
                        serverIps.add(address.getHostAddress());
                    }
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }

        ipComboBox.getItems().addAll(serverIps);

        portTextField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String s, String t1) {
                if (ipComboBox.getSelectionModel().getSelectedItem() != null &&
                        detectInteger(portTextField.getText())) {
                    startServerButton.setDisable(false);
                } else {
                    startServerButton.setDisable(true);
                }
            }
        });

        ipComboBox.valueProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observableValue, Object o, Object t1) {
                if (ipComboBox.getSelectionModel().getSelectedItem() != null &&
                    detectInteger(portTextField.getText())) {
                    startServerButton.setDisable(false);
                } else {
                    startServerButton.setDisable(true);
                }
            }
        });
    }

    private final boolean checkIPv4(final String ip) {
        boolean isIPv4;
        try {
            final InetAddress inet = InetAddress.getByName(ip);
            isIPv4 = inet.getHostAddress().equals(ip)
                    && inet instanceof Inet4Address;
        } catch (final UnknownHostException e) {
            isIPv4 = false;
        }
        return isIPv4;
    }

    private void prepareServer(String ip, int port) throws RemoteException, NotBoundException{
        cloudGraphServer = new CloudGraphServer(cloudGraphLogArea);
        cloudGraphServer.startServer(ip,port);

        cloudGraphServerInfo = ((TextArea) root.lookup("#cloudGraphServerInfo"));
        cloudGraphServerInfo.setStyle("-fx-text-fill: #00B4F4; -fx-border-color: white; -fx-border-width: 2px");
        Region infoContent = ((Region) cloudGraphServerInfo.lookup(".content"));
        infoContent.setStyle("-fx-background-color: black");

        cloudGraphServerInfo.appendText("Server Port: " + ip + "\n");
        cloudGraphServerInfo.appendText("Server Ip  : " + port);
    }

    private void prepareLogArea() {
        cloudGraphLogArea = ((TextArea) root.lookup("#cloudGraphLogScrollPane #cloudGraphLogArea"));
        cloudGraphLogArea.setStyle("-fx-text-fill: #00B4F4");
        Region logContent = ((Region) cloudGraphLogArea.lookup(".content"));
        logContent.setStyle("-fx-background-color: black");
    }

    private void prepareServerTrafficGraph() {
        XYChart.Series onlineUsersNum = new XYChart.Series();

        cloudGraphServerTrafficGraph = ((LineChart<Number, Number>) root.lookup("#cloudGraphServerTrafficGraph"));
        cloudGraphServerTrafficGraph.setStyle("-fx-border-width: 2px; -fx-border-color: white");
        NumberAxis xAxis = ((NumberAxis) cloudGraphServerTrafficGraph.lookup("#cloudGraphServerTrafficGraphXAxis"));
        NumberAxis yAxis = ((NumberAxis) cloudGraphServerTrafficGraph.lookup("#cloudGraphServerTrafficGraphYAxis"));

        xAxis.setLowerBound(0);
        xAxis.setUpperBound(60);
        xAxis.setAutoRanging(false);
        xAxis.setLabel("Seconds");

        yAxis.setLabel("People");

        cloudGraphServerTrafficGraph.setLegendVisible(false);

        cloudGraphServerTrafficGraph.getData().add(onlineUsersNum);

        ScheduledExecutorService graphPlotter = Executors.newSingleThreadScheduledExecutor();

        graphPlotter.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        if (serverTrafficTime == 60) {
                            cloudGraphServerTrafficGraph.getData().get(0).getData().clear();
                            serverTrafficTime = 0;
                        }

                        cloudGraphServerTrafficGraph.getData().get(0).getData()
                                .add(new XYChart.Data(serverTrafficTime,cloudGraphServer.getOnlineUsersNum()));
                    }
                });
                ++serverTrafficTime;
            }
        },0,1,TimeUnit.SECONDS);
    }
    
    private void prepareOnlineUsersList() {
        cloudGraphOnlineUsersList = ((ListView<String>) root.lookup("#cloudGraphOnlineUsersList"));

        ScheduledExecutorService onlineUsersListUpdater = Executors.newSingleThreadScheduledExecutor();

        onlineUsersListUpdater.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        ObservableList<String> onlineUsersList = FXCollections.observableArrayList(cloudGraphServer.getOnlineUsers());
                        cloudGraphOnlineUsersList.setItems(onlineUsersList);
                    }
                });
            }
        },0,1, TimeUnit.SECONDS);
    }

    private void startClock() {
        clock = ((Label) root.lookup("#cloudGraphClock"));
        clock.setStyle("-fx-background-color: black; -fx-border-color: white; -fx-border-width: 2px");

        ScheduledExecutorService clockService = Executors.newSingleThreadScheduledExecutor();
        clockService.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        LocalTime now = LocalTime.now();
                        clock.setText(" " + convertClockStr(now.getHour() + 1,true) + ":" +
                                            convertClockStr(now.getMinute(),false)+ ":" +
                                            convertClockStr(now.getSecond(),false));
                    }
                });
            }
        }, 0, 500, TimeUnit.MILLISECONDS);
    }

    private boolean detectInteger(String integerVal) {
        try {
            Integer.parseInt(integerVal);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    private String convertClockStr(int value, boolean hour) {
        if (!hour) {
            return value < 10 ? "0" + value : "" + value;
        } else {
            if (value < 10)
                return "0" + value;
            else if (value == 24)
                return "00";
            else
                return "" + value;
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
