import java.util.List;

/**
 * Created by mpolatcan on 24.12.2016.
 */
public interface Graph<T> {
    Graph<T> addVertex(T val);
    Graph<T> removeVertex(T val) throws CloudGraphExceptions.VertexNotFoundException;
    Graph<T> addEdge(T val, T neighborVal, double edgeVal) throws CloudGraphExceptions.VertexNotFoundException;
    Graph<T> removeEdge(T val, T neighborVal) throws CloudGraphExceptions.EdgeNotFoundException;
    Edge<T> getEdge(T val, T neighborVal) throws CloudGraphExceptions.EdgeNotFoundException;
    Vertex<T> getVertex(T val) throws CloudGraphExceptions.VertexNotFoundException;
    List<Vertex<T>> getVertices();
    List<Edge<T>> getEdges();
    int getVertexSize();
}
