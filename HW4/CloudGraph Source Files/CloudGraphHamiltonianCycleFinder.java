import java.util.ArrayList;
import java.util.List;

/**
 * Created by mpolatcan on 24.12.2016.
 */
public class CloudGraphHamiltonianCycleFinder<T> {
    private List<Vertex<T>> vertices;
    private Graph<T> graph;

    public CloudGraphHamiltonianCycleFinder(Graph<T> graph) {
        this.graph = graph;
        vertices = graph.getVertices();
    }

    public ArrayList<Vertex<T>> findHamiltonianCycle(T source) throws CloudGraphExceptions.VertexNotFoundException{
        ArrayList<Vertex<T>> hamiltonianCycle = new ArrayList<>();

        for (int i = 0; i < graph.getVertexSize() + 1; i++) {
            hamiltonianCycle.add(new CloudGraphVertex<T>(null));
        }

        hamiltonianCycle.set(0,graph.getVertex(source));

        if (findHamiltonianCycleUtil(hamiltonianCycle, 1)) {
           return hamiltonianCycle;
        } else {
            return null;
        }
    }

    private boolean findHamiltonianCycleUtil(ArrayList<Vertex<T>> path, Integer pos) {
        if (pos == graph.getVertexSize()) {
            try {
                graph.getEdge(path.get(pos - 1).getVertexValue(), path.get(0).getVertexValue());
                path.set(path.size() - 1, path.get(0));
                return true;
            } catch (CloudGraphExceptions.EdgeNotFoundException ex) {
                return false;
            }
        }

        for (int i = 0; i < graph.getVertexSize(); i++) {
            if (isCorrect(vertices.get(i), path, pos)) {
                path.set(pos,vertices.get(i));

                if (findHamiltonianCycleUtil(path,pos+1))
                    return true;

                path.remove(pos);
            }
        }

        return false;
    }

    private boolean isCorrect(Vertex<T> vertex, ArrayList<Vertex<T>> path, Integer pos) {
        try {
            graph.getEdge(path.get(pos-1).getVertexValue(),vertex.getVertexValue());
        } catch (CloudGraphExceptions.EdgeNotFoundException ex) {
            return false;
        }

        for (int i = 0; i < pos; i++) {
            if (path.get(i).equals(vertex)) {
                return false;
            }
        }

        return true;
    }

    public void setGraph(Graph<T> graph) {
        this.graph = graph;
    }
}
