import java.io.Serializable;

/**
 * Created by mpolatcan on 24.12.2016.
 */
public class CloudGraphEdge<T> implements Edge<T>, Serializable{
    private double edgeVal;
    private Vertex<T> vertex;
    private Vertex<T> neighborVertex;

    public CloudGraphEdge(Vertex<T> vertex, Vertex<T> neighborVertex, double edgeVal) {
        this.vertex = vertex;
        this.neighborVertex = neighborVertex;
        this.edgeVal = edgeVal;
    }

    @Override
    public void setVertex(Vertex<T> x) {
        vertex = x;
    }

    @Override
    public void setNeighborVertex(Vertex<T> x) {
        neighborVertex = x;
    }

    @Override
    public double getEdgeValue() {
        return edgeVal;
    }

    @Override
    public void setEdgeVal(double val) {
        edgeVal = val;
    }

    @Override
    public Vertex<T> getVertex() {
        return vertex;
    }

    @Override
    public Vertex<T> getNeighborVertex() {
        return neighborVertex;
    }

    @Override
    public String toString() {
        return String.format("%s -> %s, W: %.2f", vertex.getVertexValue(), neighborVertex.getVertexValue()
                        , edgeVal);
    }
}
