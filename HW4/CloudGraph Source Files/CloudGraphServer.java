import javafx.scene.control.TextArea;

import java.io.*;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mpolatcan on 24.12.2016.
 */
public class CloudGraphServer implements GraphComputer {
    private List<CloudGraphUser> users;
    private List<String> onlineUsers;
    private TextArea cloudGraphLogArea;

    public CloudGraphServer(TextArea cloudGraphLogArea) {
        super();
        this.cloudGraphLogArea = cloudGraphLogArea;
        users = new ArrayList<>();
        onlineUsers = new ArrayList<>();
    }

    public int getOnlineUsersNum() {
        return onlineUsers.size();
    }

    public List<String> getOnlineUsers() {
        return onlineUsers;
    }

    public void startServer(String ip, int port) throws RemoteException, NotBoundException{
        // Load registered user database
        loadFromDatabase();

        String hostName = CloudGraphConstants.BULUT_CIZGE_HOSTNAME;
        System.setProperty("java.rmi.server.hostname", ip);
        GraphComputer graphComputerStub = ((GraphComputer)
                UnicastRemoteObject.exportObject(this, port));
        Registry registry = LocateRegistry.createRegistry(port);
        registry.rebind(hostName,graphComputerStub);
    }

    @Override
    public void login(String username, String password) throws RemoteException {
        System.out.println(username);
        onlineUsers.add(username);
        System.out.println(cloudGraphLogArea);
        cloudGraphLogArea.appendText("" + convertClockStr(LocalTime.now().getHour() + 1,true) + ":" +
                convertClockStr(LocalTime.now().getMinute(),false) + ":" + convertClockStr(LocalTime.now().getSecond(),false) +
                " -> User \"" + username + "\" logged in to server\n");
    }

    @Override
    public void logout(String username) throws RemoteException {
        onlineUsers.remove(username);
        cloudGraphLogArea.appendText("" + convertClockStr(LocalTime.now().getHour() + 1,true) + ":" +
                convertClockStr(LocalTime.now().getMinute(),false) + ":" + convertClockStr(LocalTime.now().getSecond(),false) +
                " -> User \"" + username + "\" logged out from server\n");
    }

    @Override
    public void addAccount(String email, String username, String password, Integer creditAmount)
            throws RemoteException {
        users.add(new CloudGraphUser(email,username,password,creditAmount));
        onlineUsers.add(username);
        cloudGraphLogArea.appendText("" + convertClockStr(LocalTime.now().getHour() + 1,true) + ":" +
            convertClockStr(LocalTime.now().getMinute(),false) + ":" + convertClockStr(LocalTime.now().getSecond(),false) +
            " -> New user \"" + username + "\" has arrived!\n");
        saveToDatabase();
    }

    @Override
    public Integer getCreditAmount(String username) throws RemoteException {
        CloudGraphUser user = findUser(username);

        return user.getCreditAmount();
    }

    @Override
    public void buyCredits(String username, Integer creditAmount) throws RemoteException {
        CloudGraphUser user = findUser(username);

        user.setCreditAmount(user.getCreditAmount() + creditAmount);

        cloudGraphLogArea.appendText("" + convertClockStr(LocalTime.now().getHour() + 1,true) + ":" +
                convertClockStr(LocalTime.now().getMinute(),false) + ":" + convertClockStr(LocalTime.now().getSecond(),false)
                + " -> User \"" + username + "\" has bought " + creditAmount + " credits\n");

        saveToDatabase();
    }

    // Load user information from database
    private void loadFromDatabase() {
        try {
            FileInputStream fileInputStream = new FileInputStream("BulutCizgeUsers.bin");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);

            while (true) {
                try {
                    users.add(((CloudGraphUser) objectInputStream.readObject()));
                } catch (EOFException ex) {
                    break;
                } catch (ClassNotFoundException ex) {
                    System.out.println(ex.getMessage());
                }
            }

            objectInputStream.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    private void saveToDatabase() {
        try
        {
            FileOutputStream fileOutputStream = new FileOutputStream("BulutCizgeUsers.bin");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);

            for (CloudGraphUser user : users) {
                objectOutputStream.writeObject(user);
            }

            objectOutputStream.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public void checkNewAccountInfo(String email, String username)
            throws RemoteException, CloudGraphExceptions.UserAlreadyHaveAccountException {
        for (CloudGraphUser user : users) {
            if (user.getEmail().equals(email) && user.getUsername().equals(username)) {
                throw new CloudGraphExceptions.UserAlreadyHaveAccountException("Username " +
                        "and email are invalid");
            } else if (user.getEmail().equals(email)) {
                throw new CloudGraphExceptions.UserAlreadyHaveAccountException("Email invalid");
            } else if (user.getUsername().equals(username)) {
                throw new CloudGraphExceptions.UserAlreadyHaveAccountException("Username invalid");
            }
        }
    }

    @Override
    public void checkLoginInfo(String username, String password)
            throws RemoteException, CloudGraphExceptions.InvalidLoginCredentialsException {
        for (CloudGraphUser user : users) {
            if (user.getUsername().equals(username) &&
                    user.getPassword().equals(password)) {
                return;
            }
        }


        if (findUser(username) != null) {
            throw new CloudGraphExceptions.InvalidLoginCredentialsException("Password invalid");
        } else {
            throw new CloudGraphExceptions.InvalidLoginCredentialsException("Username invalid");
        }
    }

    private CloudGraphUser findUser(String username) {
        for (CloudGraphUser user : users) {
            if (user.getUsername().equals(username)) {
                return user;
            }
        }

        return null;
    }

    private boolean canUserAffordCredit(String username, Integer decreaseAmount) {
        CloudGraphUser user = findUser(username);

        if (user.getCreditAmount() >= decreaseAmount) {
            user.setCreditAmount(user.getCreditAmount() - decreaseAmount);
            saveToDatabase();
            return true;
        } else {
            return false;
        }
    }

    private String convertClockStr(int value, boolean hour) {
        if (!hour) {
            return value < 10 ? "0" + value : "" + value;
        } else {
            if (value < 10)
                return "0" + value;
            else if (value == 24)
                return "00";
            else
                return "" + value;
        }
    }

    @Override
    public <T> List<Vertex<T>> shortestPathFinderService(String username, Graph<T> graph, T sourceVal, T targetVal)
            throws RemoteException,
                   CloudGraphExceptions.VertexNotFoundException,
                   CloudGraphExceptions.UnsufficientCreditsException {
        cloudGraphLogArea.appendText("" + convertClockStr(LocalTime.now().getHour() + 1,true) + 
                ":" + convertClockStr(LocalTime.now().getMinute(),false) + ":"
                + convertClockStr(LocalTime.now().getSecond(),false) + " -> \"" + username + "\" requested find " +
                "shortest path in" + " graph between source \"" + sourceVal + "\" and target \"" + targetVal + "\"...\n");

        if (canUserAffordCredit(username,5)) {
            CloudGraphShortestPathFinder<T> shortestPathFinder = new CloudGraphShortestPathFinder<T>(graph);
            List<Vertex<T>> result = shortestPathFinder.findShortestPath(sourceVal, targetVal);

            cloudGraphLogArea.appendText("" + convertClockStr(LocalTime.now().getHour() + 1,true) + ":"
                    + convertClockStr(LocalTime.now().getMinute(),false) + ":" + convertClockStr(LocalTime.now().getSecond(),false)
                    + " -> \"" + username + "\" shortest path founded between " + "source \"" + sourceVal + "\" and target \""
                    + targetVal + "\"...\n");


            return result;
        } else {
            throw new CloudGraphExceptions.UnsufficientCreditsException("You haven't enough credits for" +
                    " compute shortest path between two nodes");
        }
    }

    @Override
    public <T> List<Vertex<T>> hamiltonianCycleFinderService(String username, Graph<T> graph, T source)
            throws RemoteException,
                   CloudGraphExceptions.VertexNotFoundException,
                   CloudGraphExceptions.UnsufficientCreditsException {
        cloudGraphLogArea.appendText("" + convertClockStr(LocalTime.now().getHour() + 1,true) + ":" + convertClockStr(LocalTime.now().getMinute(),false) + ":"
                + convertClockStr(LocalTime.now().getSecond(),false) + " -> \"" + username + "\" requested find hamiltonian cycle in" +
                " graph from source \"" + source + "\"...\n");

        if (canUserAffordCredit(username,10)) {
            CloudGraphHamiltonianCycleFinder<T> hamiltonianCycleFinder = new CloudGraphHamiltonianCycleFinder<T>(graph);
            List<Vertex<T>> result = hamiltonianCycleFinder.findHamiltonianCycle(source);

            cloudGraphLogArea.appendText("" + convertClockStr(LocalTime.now().getHour() + 1,true) + ":" + convertClockStr(LocalTime.now().getMinute(),false) + ":"
                    + convertClockStr(LocalTime.now().getSecond(),false) + " -> \"" + username + "\" founded hamiltonian cycle in" +
                    " graph from source \"" + source + "\"...\n");

            return result;
        } else {
            throw new CloudGraphExceptions.UnsufficientCreditsException("You haven't enough credits for" +
                    " compute hamilton cycle from source node");
        }
    }
}