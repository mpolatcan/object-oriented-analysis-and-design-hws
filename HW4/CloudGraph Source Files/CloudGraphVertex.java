import java.io.Serializable;

/**
 * Created by mpolatcan on 24.12.2016.
 */
public class CloudGraphVertex<T> implements Vertex<T>, Serializable{
    private T vertexVal;

    public CloudGraphVertex(T val) {
        vertexVal = val;
    }

    @Override
    public void setVertexValue(T val) {
        vertexVal = val;
    }

    @Override
    public T getVertexValue() {
        return vertexVal;
    }

    @Override
    public String toString() {
        return "" + vertexVal;
    }
}
