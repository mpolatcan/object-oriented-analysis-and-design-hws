/**
 * Created by mpolatcan on 25.12.2016.
 */
public interface CloudGraphConstants {
    String GRAPH_DATA_TYPE_INTEGER = "Integer";
    String GRAPH_DATA_TYPE_DOUBLE = "Double";
    String GRAPH_DATA_TYPE_FLOAT = "Float";
    String GRAPH_DATA_TYPE_STRING = "String";
    String SHORTEST_PATH = "Shortest Path";
    String HAMILTONIAN_CYCLE = "Hamiltonian Cycle";
    String BULUT_CIZGE_HOSTNAME = "Bulut Cizgi Company - Graph Computer";
}
