/**
 * Created by mpolatcan on 28.12.2016.
 */
public class CloudGraphExceptions {
    public static class VertexNotFoundException extends Exception {
        public VertexNotFoundException(String message) {
            super(message);
        }
    }

    public static class EdgeNotFoundException extends Exception {
        public EdgeNotFoundException(String message) { super(message); }
    }

    public static class UserAlreadyHaveAccountException extends Exception {
        public UserAlreadyHaveAccountException(String message) {
            super(message);
        }
    }

    public static class InvalidLoginCredentialsException extends Exception {
        public InvalidLoginCredentialsException(String message) { super(message);}
    }

    public static class UnsufficientCreditsException extends Exception {
        public UnsufficientCreditsException(String message) { super(message);}
    }
}
