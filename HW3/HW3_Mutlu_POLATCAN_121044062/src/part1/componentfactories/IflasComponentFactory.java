package part1.componentfactories;

import part1.components.*;

/**
 * Created by mpolatcan-gyte_cse on 11.12.2016.
 */
public interface IflasComponentFactory {
    IflasBattery getIflasDeluxeBattery();
    IflasBattery getMaximumEffortBattery();
    IflasBattery getIIAmanIflasBattery();
    IflasCamera getIflasDeluxeCamera();
    IflasCamera getMaximumEffortCamera();
    IflasCamera getIIAmanIflasCamera();
    IflasPhoneCase getIflasDeluxePhoneCase();
    IflasPhoneCase getMaximumEffortPhoneCase();
    IflasPhoneCase getIIAmanIflasPhoneCase();
    IflasCpu getIflasDeluxeCpu();
    IflasCpu getMaximumEffortCpu();
    IflasCpu getIIAmanIflasCpu();
    IflasDisplay getIflasDeluxeDisplay();
    IflasDisplay getMaximumEffortDisplay();
    IflasDisplay getIIAmanIflasDisplay();
    IflasRam getIflasDeluxeRam();
    IflasRam getMaximumEffortRam();
    IflasRam getIIAmanIflasRam();
    IflasStorage getIflasDeluxeStorage();
    IflasStorage getMaximumEffortStorage();
    IflasStorage getIIAmanIflasStorage();
}
