package part1.componentfactories;

import part1.components.*;

/**
 * Created by mpolatcan-gyte_cse on 11.12.2016.
 */
public class IflasGlobalComponentFactory implements IflasComponentFactory {
    @Override
    public IflasBattery getIflasDeluxeBattery() {
        return new IflasBattery(20,2800,"Lithium-Cobalt");
    }

    @Override
    public IflasBattery getMaximumEffortBattery() {
        return new IflasBattery(27,3600,"Lithium-Boron");
    }

    @Override
    public IflasBattery getIIAmanIflasBattery() {
        return new IflasBattery(16,2000,"Lithium-Ion");
    }

    @Override
    public IflasCamera getIflasDeluxeCamera() {
        return new IflasCamera(12,5,2);
    }

    @Override
    public IflasCamera getMaximumEffortCamera() {
        return new IflasCamera(12,8,4);
    }

    @Override
    public IflasCamera getIIAmanIflasCamera() {
        return new IflasCamera(8,5,2);
    }

    @Override
    public IflasPhoneCase getIflasDeluxePhoneCase() {
        return new IflasPhoneCase(149,73,7.7,
                "waterproof,aluminum","50cm");
    }

    @Override
    public IflasPhoneCase getMaximumEffortPhoneCase() {
        return new IflasPhoneCase(151,73,7.7,
                "dustproof,waterproof,aluminum","1m");
    }

    @Override
    public IflasPhoneCase getIIAmanIflasPhoneCase() {
        return new IflasPhoneCase(143,69,7.3,
                "waterproof,plastic","25cm");
    }

    @Override
    public IflasCpu getIflasDeluxeCpu() {
        return new IflasCpu(2.2,2);
    }

    @Override
    public IflasCpu getMaximumEffortCpu() {
        return new IflasCpu(2.8,4);
    }

    @Override
    public IflasCpu getIIAmanIflasCpu() {
        return new IflasCpu(2.2,2);
    }

    @Override
    public IflasDisplay getIflasDeluxeDisplay() {
        return new IflasDisplay(5.3,24);
    }

    @Override
    public IflasDisplay getMaximumEffortDisplay() {
        return new IflasDisplay(5.5,64);
    }

    @Override
    public IflasDisplay getIIAmanIflasDisplay() {
        return new IflasDisplay(4.5,16);
    }

    @Override
    public IflasRam getIflasDeluxeRam() {
        return new IflasRam(6);
    }

    @Override
    public IflasRam getMaximumEffortRam() {
        return new IflasRam(8);
    }

    @Override
    public IflasRam getIIAmanIflasRam() {
        return new IflasRam(4);
    }

    @Override
    public IflasStorage getIflasDeluxeStorage() {
        return new IflasStorage(32,32);
    }

    @Override
    public IflasStorage getMaximumEffortStorage() {
        return new IflasStorage(64,64);
    }

    @Override
    public IflasStorage getIIAmanIflasStorage() {
        return new IflasStorage(16,16);
    }
}
