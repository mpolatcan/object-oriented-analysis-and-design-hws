package part1;

import part1.marketfactories.IflasEuropeMarketFactory;
import part1.marketfactories.IflasGlobalMarketFactory;
import part1.marketfactories.IflasPhoneFactory;
import part1.marketfactories.IflasTurkeyMarketFactory;

/**
 * Created by mpolatcan-gyte_cse on 11.12.2016.
 */
public class Part1Test {
    public static void main(String[] args) {
        System.out.printf("************ TURKEY PHONES ************\n");
        IflasPhoneFactory phoneFactory = new IflasTurkeyMarketFactory();

        System.out.println(phoneFactory.getMaximumEffortPhone());
        System.out.println(phoneFactory.getIflasDeluxePhone());
        System.out.println(phoneFactory.getIIAmanIflasPhone());

        System.out.println("\n************ GLOBAL PHONES ************\n");
        phoneFactory = new IflasGlobalMarketFactory();
        System.out.println(phoneFactory.getMaximumEffortPhone());
        System.out.println(phoneFactory.getIflasDeluxePhone());
        System.out.println(phoneFactory.getIIAmanIflasPhone());

        System.out.printf("\n*********** EUROPE PHONES *************\n");
        phoneFactory = new IflasEuropeMarketFactory();
        System.out.println(phoneFactory.getMaximumEffortPhone());
        System.out.println(phoneFactory.getIflasDeluxePhone());
        System.out.println(phoneFactory.getIIAmanIflasPhone());
    }
}
