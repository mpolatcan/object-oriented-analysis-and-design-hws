package part1.components;

/**
 * Created by mpolatcan-gyte_cse on 11.12.2016.
 */
public class IflasPhoneCase {
    private double caseWidth;
    private double caseHeight;
    private double caseLength;
    private String additionalSpecs;
    private String waterproofUpTo;

    public IflasPhoneCase(double caseLength,
                          double caseWidth,
                          double caseHeight,
                          String additionalSpecs,
                          String waterproofUpTo) {
        this.caseWidth = caseWidth;
        this.caseHeight = caseHeight;
        this.caseLength = caseLength;
        this.additionalSpecs = additionalSpecs;
        this.waterproofUpTo = waterproofUpTo;
    }

    public void setCaseWidth(double caseWidth) {
        this.caseWidth = caseWidth;
    }

    public void setCaseHeight(double caseHeight) {
        this.caseHeight = caseHeight;
    }

    public void setCaseLength(double caseLength) {
        this.caseLength = caseLength;
    }

    public void setAdditionalSpecs(String additionalSpecs) {
        this.additionalSpecs = additionalSpecs;
    }

    public void setWaterproofUpTo(String waterproofUpTo) {
        this.waterproofUpTo = waterproofUpTo;
    }

    public double getCaseWidth() {
        return caseWidth;
    }

    public double getCaseHeight() {
        return caseHeight;
    }

    public double getCaseLength() {
        return caseLength;
    }

    public String getAdditionalSpecs() {
        return additionalSpecs;
    }

    public String getWaterproofUpTo() {
        return waterproofUpTo;
    }

    @Override
    public String toString() {
        return String.format("%.2fx%.0fx%.2f mm,%s,Waterproof up to %s", caseLength,
                caseWidth, caseHeight, additionalSpecs, waterproofUpTo);
    }
}
