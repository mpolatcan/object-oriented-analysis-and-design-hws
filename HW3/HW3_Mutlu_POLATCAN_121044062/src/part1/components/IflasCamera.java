package part1.components;

/**
 * Created by mpolatcan-gyte_cse on 11.12.2016.
 */
public class IflasCamera {
    private int frontCameraResolution;
    private int rearCameraResolution;
    private int opticalZoomAmount;

    public IflasCamera(int frontCameraResolution,
                       int rearCameraResolution,
                       int opticalZoomAmount) {
        this.frontCameraResolution = frontCameraResolution;
        this.rearCameraResolution = rearCameraResolution;
        this.opticalZoomAmount = opticalZoomAmount;
    }

    public void setRearCameraResolution(int rearCameraResolution) {
        this.rearCameraResolution = rearCameraResolution;
    }

    public void setFrontCameraResolution(int frontCameraResolution) {
        this.frontCameraResolution = frontCameraResolution;
    }

    public void setOpticalZoomAmount(int opticalZoomAmount) {
        this.opticalZoomAmount = opticalZoomAmount;
    }

    public int getRearCameraResolution() {
        return rearCameraResolution;
    }

    public int getFrontCameraResolution() {
        return frontCameraResolution;
    }

    public int getOpticalZoomAmount() {
        return opticalZoomAmount;
    }

    @Override
    public String toString() {
        return String.format("%dMp front, %dMp rear, Opt. zoom x%d",
                frontCameraResolution, rearCameraResolution, opticalZoomAmount);
    }
}
