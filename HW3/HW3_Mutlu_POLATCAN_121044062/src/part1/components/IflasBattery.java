package part1.components;

/**
 * Created by mpolatcan-gyte_cse on 11.12.2016.
 */
public class IflasBattery {
    private int batteryLife;
    private int batteryCapacity;
    private String batteryType;

    public IflasBattery(int batteryLife, int batteryCapacity, String batteryType) {
        this.batteryLife = batteryLife;
        this.batteryCapacity = batteryCapacity;
        this.batteryType = batteryType;
    }

    public void setBatteryCapacity(int batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }

    public void setBatteryLife(int batteryLife) {
        this.batteryLife = batteryLife;
    }

    public void setBatteryType(String batteryType) {
        this.batteryType = batteryType;
    }

    public int getBatteryCapacity() {
        return batteryCapacity;
    }

    public int getBatteryLife() {
        return batteryLife;
    }

    public String getBatteryType() {
        return batteryType;
    }

    @Override
    public String toString() {
        return String.format("%dh, %dmAh, %s", batteryLife, batteryCapacity,
                batteryType);
    }
}
