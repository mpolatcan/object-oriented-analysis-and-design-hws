package part1.components;

/**
 * Created by mpolatcan-gyte_cse on 11.12.2016.
 */
public class IflasStorage {
    private int storageAmount;
    private int maxStorageAmount;

    public IflasStorage(int storageAmount, int maxStorageAmount) {
        this.storageAmount = storageAmount;
        this.maxStorageAmount = maxStorageAmount;
    }

    public void setStorageAmount(int storageAmount) {
        this.storageAmount = storageAmount;
    }

    public void setMaxStorageAmount(int maxStorageAmount) {
        this.maxStorageAmount = maxStorageAmount;
    }

    public int getStorageAmount() {
        return storageAmount;
    }

    public int getMaxStorageAmount() {
        return maxStorageAmount;
    }

    @Override
    public String toString() {
        return String.format("MicroSD support, %dGB, Max %dGB",
                storageAmount, maxStorageAmount);
    }
}
