package part1.components;

/**
 * Created by mpolatcan-gyte_cse on 11.12.2016.
 */
public class IflasCpu {
    private double clockSpeed;
    private int coreNumber;

    public IflasCpu(double clockSpeed, int coreNumber) {
        this.clockSpeed = clockSpeed;
        this.coreNumber = coreNumber;
    }

    public void setClockSpeed(double clockSpeed) {
        this.clockSpeed = clockSpeed;
    }

    public void setCoreNumber(int coreNumber) {
        this.coreNumber = coreNumber;
    }

    public double getClockSpeed() {
        return clockSpeed;
    }

    public int getCoreNumber() {
        return coreNumber;
    }

    @Override
    public String toString() {
        return String.format("%.2f GHZ - %d cores", clockSpeed, coreNumber);
    }
}
