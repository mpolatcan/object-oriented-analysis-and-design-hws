package part1.components;

/**
 * Created by mpolatcan-gyte_cse on 11.12.2016.
 */
public class IflasDisplay {
    private double displaySize;
    private int displayDepth;

    public IflasDisplay(double displaySize, int displayDepth) {
        this.displaySize = displaySize;
        this.displayDepth = displayDepth;
    }

    public void setDisplaySize(double displaySize) {
        this.displaySize = displaySize;
    }

    public void setDisplayDepth(int displayDepth) {
        this.displayDepth = displayDepth;
    }

    public double getDisplaySize() {
        return displaySize;
    }

    public int getDisplayDepth() {
        return displayDepth;
    }

    @Override
    public String toString() {
        return String.format("%.1f inches, %d bit", displaySize,
                displayDepth);
    }
}
