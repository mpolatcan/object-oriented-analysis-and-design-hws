package part1.components;

/**
 * Created by mpolatcan-gyte_cse on 11.12.2016.
 */
public class IflasRam {
    private int memoryAmount;

    public IflasRam(int memoryAmount) {
        this.memoryAmount = memoryAmount;
    }

    public void setMemoryAmount(int memoryAmount) {
        this.memoryAmount = memoryAmount;
    }

    public int getMemoryAmount() {
        return memoryAmount;
    }

    @Override
    public String toString() {
        return String.format("%dGB", memoryAmount);
    }
}
