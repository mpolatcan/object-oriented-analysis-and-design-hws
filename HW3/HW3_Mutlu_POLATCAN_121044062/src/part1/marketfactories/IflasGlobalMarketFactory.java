package part1.marketfactories;

import part1.basephone.IflasPhone;
import part1.basephone.IflasPhoneProducer;
import part1.componentfactories.IflasGlobalComponentFactory;

/**
 * Created by mpolatcan-gyte_cse on 11.12.2016.
 */
public class IflasGlobalMarketFactory implements IflasPhoneFactory {
    private IflasPhoneProducer producer;

    public IflasGlobalMarketFactory() {
        producer = new IflasPhoneProducer(new IflasGlobalComponentFactory());
    }

    @Override
    public IflasPhone getIflasDeluxePhone() {
        return producer.produceIflasDeluxePhone();
    }

    @Override
    public IflasPhone getMaximumEffortPhone() {
        return producer.produceMaximumEffortPhone();
    }

    @Override
    public IflasPhone getIIAmanIflasPhone() {
        return producer.produceIIAmanIflasPhone();
    }
}
