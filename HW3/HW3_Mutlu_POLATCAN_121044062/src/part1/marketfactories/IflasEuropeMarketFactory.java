package part1.marketfactories;

import part1.basephone.IflasPhone;
import part1.basephone.IflasPhoneProducer;
import part1.componentfactories.IflasEuropeComponentFactory;

/**
 * Created by mpolatcan-gyte_cse on 11.12.2016.
 */
public class IflasEuropeMarketFactory implements IflasPhoneFactory {
    private IflasPhoneProducer producer;

    public IflasEuropeMarketFactory() {
        producer = new IflasPhoneProducer(new IflasEuropeComponentFactory());
    }

    @Override
    public IflasPhone getIflasDeluxePhone() {
        return producer.produceIflasDeluxePhone();
    }

    @Override
    public IflasPhone getMaximumEffortPhone() {
        return producer.produceMaximumEffortPhone();
    }

    @Override
    public IflasPhone getIIAmanIflasPhone() {
        return producer.produceIIAmanIflasPhone();
    }
}
