package part1.marketfactories;

import part1.basephone.IflasPhoneProducer;
import part1.componentfactories.IflasTurkeyComponentFactory;
import part1.basephone.IflasPhone;

/**
 * Created by mpolatcan-gyte_cse on 11.12.2016.
 */
public class IflasTurkeyMarketFactory implements IflasPhoneFactory {
    private IflasPhoneProducer producer;

    public IflasTurkeyMarketFactory() {
        producer = new IflasPhoneProducer(new IflasTurkeyComponentFactory());
    }

    @Override
    public IflasPhone getIflasDeluxePhone() {
       return producer.produceIflasDeluxePhone();
    }

    @Override
    public IflasPhone getMaximumEffortPhone() {
       return producer.produceMaximumEffortPhone();
    }

    @Override
    public IflasPhone getIIAmanIflasPhone() {
        return producer.produceIIAmanIflasPhone();
    }
}
