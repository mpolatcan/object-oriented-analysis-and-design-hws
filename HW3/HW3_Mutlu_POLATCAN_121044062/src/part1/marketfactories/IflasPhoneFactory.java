package part1.marketfactories;

import part1.basephone.IflasPhone;

/**
 * Created by mpolatcan-gyte_cse on 11.12.2016.
 */
public interface IflasPhoneFactory {
    IflasPhone getIflasDeluxePhone();
    IflasPhone getMaximumEffortPhone();
    IflasPhone getIIAmanIflasPhone();
}
