package part1.basephone;

import part1.components.*;

/**
 * Created by mpolatcan-gyte_cse on 11.12.2016.
 */
public class IflasPhone {
    private String modelName;
    private IflasBattery battery;
    private IflasCamera camera;
    private IflasPhoneCase phoneCase;
    private IflasCpu cpu;
    private IflasDisplay display;
    private IflasRam ram;
    private IflasStorage storage;

    public IflasPhone(String modelName) {
        this.modelName = modelName;
    }

    public void setRam(IflasRam ram) {
        this.ram = ram;
    }

    public void setPhoneCase(IflasPhoneCase phoneCase) {
        this.phoneCase = phoneCase;
    }

    public void setCpu(IflasCpu cpu) {
        this.cpu = cpu;
    }

    public void setBattery(IflasBattery battery) {
        this.battery = battery;
    }

    public void setCamera(IflasCamera camera) {
        this.camera = camera;
    }

    public void setDisplay(IflasDisplay display) {
        this.display = display;
    }

    public void setStorage(IflasStorage storage) {
        this.storage = storage;
    }

    public IflasBattery getBattery() {
        return battery;
    }

    public IflasCamera getCamera() {
        return camera;
    }

    public IflasPhoneCase getPhoneCase() {
        return phoneCase;
    }

    public IflasCpu getCpu() {
        return cpu;
    }

    public IflasDisplay getDisplay() {
        return display;
    }

    public IflasRam getRam() {
        return ram;
    }

    public IflasStorage getStorage() {
        return storage;
    }

    public String getModelName() {
        return modelName;
    }

    @Override
    public String toString() {
        return String.format("Model name: %s\nDisplay: %s\nBattery: %s\n" +
                "CPU: %s\nRAM: %s\nStorage: %s\nCamera: %s\nCase: %s\n",
                modelName, display, battery, cpu, ram, storage, camera, phoneCase);
    }
}
