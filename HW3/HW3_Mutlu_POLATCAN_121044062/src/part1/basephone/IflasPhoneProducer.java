package part1.basephone;

import part1.componentfactories.IflasComponentFactory;
import part1.components.*;

import java.util.concurrent.TimeUnit;

/**
 * Created by mpolatcan-gyte_cse on 11.12.2016.
 */
public class IflasPhoneProducer {
    private IflasComponentFactory componentFactory;

    public IflasPhoneProducer(IflasComponentFactory componentFactory) {
        this.componentFactory = componentFactory;
    }


    public IflasPhone produceMaximumEffortPhone() {
        IflasPhone maximumEffort = new IflasPhone("MaximumEffort");

        attachCpu(maximumEffort,componentFactory.getMaximumEffortCpu());
        attachRam(maximumEffort,componentFactory.getMaximumEffortRam());
        attachDisplay(maximumEffort,componentFactory.getMaximumEffortDisplay());
        attachBattery(maximumEffort,componentFactory.getMaximumEffortBattery());
        attachStorage(maximumEffort,componentFactory.getMaximumEffortStorage());
        attachCamera(maximumEffort,componentFactory.getMaximumEffortCamera());
        enclosePhoneCase(maximumEffort,componentFactory.getMaximumEffortPhoneCase());

        return maximumEffort;
    }

    public IflasPhone produceIflasDeluxePhone() {
        IflasPhone iflasDeluxe = new IflasPhone("IflasDeluxe");

        attachCpu(iflasDeluxe,componentFactory.getIflasDeluxeCpu());
        attachRam(iflasDeluxe,componentFactory.getIflasDeluxeRam());
        attachDisplay(iflasDeluxe,componentFactory.getIflasDeluxeDisplay());
        attachBattery(iflasDeluxe,componentFactory.getIflasDeluxeBattery());
        attachStorage(iflasDeluxe,componentFactory.getIflasDeluxeStorage());
        attachCamera(iflasDeluxe,componentFactory.getIflasDeluxeCamera());
        enclosePhoneCase(iflasDeluxe,componentFactory.getIflasDeluxePhoneCase());

        return iflasDeluxe;
    }

    public IflasPhone produceIIAmanIflasPhone() {
        IflasPhone iiAmanIflas = new IflasPhone("IIAmanIflas");

        attachCpu(iiAmanIflas,componentFactory.getIIAmanIflasCpu());
        attachRam(iiAmanIflas,componentFactory.getIIAmanIflasRam());
        attachDisplay(iiAmanIflas,componentFactory.getIIAmanIflasDisplay());
        attachBattery(iiAmanIflas,componentFactory.getIIAmanIflasBattery());
        attachStorage(iiAmanIflas,componentFactory.getIIAmanIflasStorage());
        attachCamera(iiAmanIflas,componentFactory.getIIAmanIflasCamera());
        enclosePhoneCase(iiAmanIflas,componentFactory.getIIAmanIflasPhoneCase());

        return iiAmanIflas;
    }

    private void attachStorage(IflasPhone iflasPhone, IflasStorage storage) {
        iflasPhone.setStorage(storage);
        System.out.println("Attaching Storage -> " + iflasPhone.getStorage() + "...");
        producerTimeout();
    }

    private void attachBattery(IflasPhone iflasPhone, IflasBattery battery) {
        iflasPhone.setBattery(battery);
        System.out.println("Attaching Battery -> " + iflasPhone.getBattery() + "...");
        producerTimeout();
    }

    private void attachCamera(IflasPhone iflasPhone, IflasCamera camera) {
        iflasPhone.setCamera(camera);
        System.out.println("Attaching Camera -> " + iflasPhone.getCamera() + "...");
        producerTimeout();
    }

    private void attachCpu(IflasPhone iflasPhone, IflasCpu cpu) {
        iflasPhone.setCpu(cpu);
        System.out.println("Attaching CPU -> " + iflasPhone.getCpu() + "...");
        producerTimeout();
    }

    private void attachDisplay(IflasPhone iflasPhone, IflasDisplay display) {
        iflasPhone.setDisplay(display);
        System.out.println("Attaching Display -> " + iflasPhone.getDisplay() + "...");
        producerTimeout();
    }

    private void attachRam(IflasPhone iflasPhone, IflasRam ram) {
        iflasPhone.setRam(ram);
        System.out.println("Attaching RAM -> " + iflasPhone.getRam() + "...");
        producerTimeout();
    }

    private void enclosePhoneCase(IflasPhone iflasPhone, IflasPhoneCase phoneCase) {
        iflasPhone.setPhoneCase(phoneCase);
        System.out.println("Enclosing Phone Case -> " + iflasPhone.getPhoneCase() + "...\n");
        producerTimeout();
    }

    private void producerTimeout() {
        try {
            TimeUnit.MILLISECONDS.sleep(300);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
}
