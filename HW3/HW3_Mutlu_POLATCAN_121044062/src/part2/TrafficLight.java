package part2;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by mpolatcan-gyte_cse on 11.12.2016.
 */
public class TrafficLight implements MobeseObserver{
    private TrafficLightPanel trafficLightPanel;
    private LightState lightState;
    private int counter = 0;
    private int timeoutX = 60;

    public TrafficLight(TrafficLightPanel trafficLightPanel) {
        lightState = new Red();
        this.trafficLightPanel = trafficLightPanel;

        System.out.println(lightState);
    }

    public LightState getLightState() {
        return lightState;
    }


    public void setLightState(LightState lightState) {
        this.lightState = lightState;

        trafficLightPanel.setLightState(getLightState().toString());

        System.out.println(lightState);
    }

    public void switchLightState() {
        if (getLightState().toString().equals("RED")) {
            lightStateSwitcher(16);
        } else if (getLightState().toString().equals("GREEN")) {
            lightStateSwitcher(timeoutX);
        } else if (getLightState().toString().equals("YELLOW")) {
            lightStateSwitcher(4);
        }
    }

    private void lightStateSwitcher(int timeout) {
        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();

        service.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                System.out.print("\r" + "Time: " + counter);
                trafficLightPanel.setCounter(counter);
                ++counter;
            }
        }, 0, 1, TimeUnit.SECONDS);

        try {
            TimeUnit.SECONDS.sleep(timeout);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        service.shutdown();
        counter = 0;
        trafficLightPanel.setCounter(counter);
        System.out.println();
        lightState.switchState(TrafficLight.this);
}

    @Override
    public void trafficChanged(boolean flag) {
        if (flag) {
            System.out.println("Traffic increased");
            timeoutX = 91;
            trafficLightPanel.setTrafficState("OVERWHELMED");
        } else {
            System.out.println("Traffic decreased");
            timeoutX = 61;
            trafficLightPanel.setTrafficState("NORMAL");
        }
    }
}
