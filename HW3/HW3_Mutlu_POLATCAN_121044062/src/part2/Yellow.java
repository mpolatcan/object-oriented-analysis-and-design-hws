package part2;

/**
 * Created by mpolatcan-gyte_cse on 11.12.2016.
 */
public class Yellow implements LightState {
    @Override
    public void switchState(TrafficLight trafficLight) {
        trafficLight.setLightState(new Red());
    }

    @Override
    public String toString() {
        return "YELLOW";
    }
}
