package part2;

import java.util.ArrayList;

/**
 * Created by mpolatcan-gyte_cse on 12.12.2016.
 */
public class HiTech implements MobeseObservable{
    // trafficState true if traffic increased else false
    private boolean trafficState = false;
    private ArrayList<MobeseObserver> mobeseObservers;

    public HiTech() {
        mobeseObservers = new ArrayList<>();
    }

    public void trafficIncreased() {
       trafficState = true;
       changeDetected(trafficState);
    }

    public void trafficDecreased() {
        trafficState = false;
        changeDetected(trafficState);
    }


    public boolean getTrafficState() {
        return trafficState;
    }

    @Override
    public void addObserver(MobeseObserver observer) {
        mobeseObservers.add(observer);
    }

    @Override
    public void removeObserver(MobeseObserver observer) {
        mobeseObservers.remove(observer);
    }

    @Override
    public void changeDetected(boolean trafficState) {
        for (MobeseObserver observer : mobeseObservers) {
            observer.trafficChanged(trafficState);
        }
    }
}
