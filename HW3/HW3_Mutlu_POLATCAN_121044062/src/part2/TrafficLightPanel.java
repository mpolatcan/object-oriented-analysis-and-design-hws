package part2;

import javax.swing.*;
import java.awt.*;

/**
 * Created by mpolatcan-gyte_cse on 12.12.2016.
 */
public class TrafficLightPanel extends JPanel{
    private JPanel panel;
    private String lightState = "RED";
    private String trafficState = "NORMAL";
    private int counter = 0;
    private Color redLightOff;
    private Color yellowLightOff;
    private Color greenLightOff;

    public TrafficLightPanel() {
        panel = new JPanel(new FlowLayout());
        panel.setSize(100,250);
        panel.setBackground(Color.LIGHT_GRAY);
        redLightOff = new Color(128,0,0);
        yellowLightOff = new Color(192,192,0);
        greenLightOff = new Color(0,96,0);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        drawBackground(g);
        drawOpenLights(g);
    }

    public void setLightState(String lightState) {
        this.lightState = lightState;
        repaint();
    }

    public void setCounter(int counter) {
        this.counter = counter;
        repaint();
    }

    public void setTrafficState(String trafficState) {
        this.trafficState = trafficState;
        repaint();
    }

    private void drawBackground(Graphics g) {
        g.setColor(redLightOff);
        g.fillOval(50,10,70,70);
        g.setColor(Color.BLACK);
        g.drawOval(50,10,70,70);

        g.setColor(yellowLightOff);
        g.fillOval(50,90,70,70);
        g.setColor(Color.BLACK);
        g.drawOval(50,90,70,70);

        g.setColor(greenLightOff);
        g.fillOval(50,170,70,70);
        g.setColor(Color.BLACK);
        g.drawOval(50,170,70,70);

        g.setColor(Color.BLACK);
        g.fillRect(210,24,120,120);
        g.setColor(redLightOff);
        g.fillRect(220,34,100,100);

        String counterStr;

        if (counter < 10) {
            counterStr = " " + counter;
        } else {
            counterStr = "" + counter;
        }

        g.setColor(Color.WHITE);
        g.setFont(new Font("SansSerif",Font.BOLD,70));
        g.drawString(counterStr, 232, 109);

        g.setColor(Color.BLACK);
        g.fillRect(182,180,170,55);


        if (trafficState.equals("NORMAL")) {
            g.setColor(Color.GREEN);
            g.fillRect(190,187,155,40);
            g.setColor(Color.WHITE);
            g.setFont(new Font("SansSerif", Font.BOLD, 18));
            g.drawString(trafficState, 225, 215);
        } else if (trafficState.equals("OVERWHELMED")) {
            g.setColor(Color.RED);
            g.fillRect(190,187,155,40);
            g.setColor(Color.WHITE);
            g.setFont(new Font("SansSerif", Font.BOLD, 18));
            g.drawString(trafficState, 196, 215);
        }
    }

    private void drawOpenLights(Graphics g) {
        if (lightState.equals("RED")) {
            g.setColor(Color.RED);
            g.fillOval(50,10,70,70);
            g.setColor(Color.BLACK);
            g.drawOval(50,10,70,70);
        } else if (lightState.equals("YELLOW")) {
            g.setColor(Color.YELLOW);
            g.fillOval(50,90,70,70);
            g.setColor(Color.BLACK);
            g.drawOval(50,90,70,70);
        } else if (lightState.equals("GREEN")) {
            g.setColor(Color.GREEN);
            g.fillOval(50,170,70,70);
            g.setColor(Color.BLACK);
            g.drawOval(50,170,70,70);
        }
    }
}
