package part2;

/**
 * Created by mpolatcan-gyte_cse on 11.12.2016.
 */
public class Green implements LightState {
    @Override
    public void switchState(TrafficLight trafficLight) {
        trafficLight.setLightState(new Yellow());
    }

    @Override
    public String toString() {
        return "GREEN";
    }
}
