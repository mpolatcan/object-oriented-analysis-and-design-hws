package part2;

/**
 * Created by mpolatcan-gyte_cse on 11.12.2016.
 */
public interface LightState {
    void switchState(TrafficLight trafficLight);
}
