package part2;

/**
 * Created by mpolatcan-gyte_cse on 11.12.2016.
 */
public class Red implements LightState{
    @Override
    public void switchState(TrafficLight trafficLight) {
        trafficLight.setLightState(new Green());
    }

    @Override
    public String toString() {
        return "RED";
    }
}
