package part2;

/**
 * Created by mpolatcan-gyte_cse on 12.12.2016.
 */
public interface MobeseObservable {
    void addObserver(MobeseObserver observer);
    void removeObserver(MobeseObserver observer);
    void changeDetected(boolean trafficState);
}
