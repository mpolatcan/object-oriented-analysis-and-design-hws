package part2;

import javax.swing.*;

/**
 * Created by mpolatcan-gyte_cse on 11.12.2016.
 */
public class Part2Test {
    public static void main(String[] args) {
        TrafficLightPanel trafficLightGUI = new TrafficLightPanel();
        JFrame frame = new JFrame("Traffic Light with Mobese");
        frame.setSize(400, 290);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(trafficLightGUI);
        frame.setVisible(true);

        TrafficLight trafficLight = new TrafficLight(trafficLightGUI);
        HiTech mobeseCamera = new HiTech();
        mobeseCamera.addObserver(trafficLight);

        while (true) {
            trafficLight.switchLightState();
            mobeseCamera.trafficIncreased();
            trafficLight.switchLightState();
            trafficLight.switchLightState();
            trafficLight.switchLightState();
            mobeseCamera.trafficDecreased();
            trafficLight.switchLightState();
            trafficLight.switchLightState();
        }
    }
}
