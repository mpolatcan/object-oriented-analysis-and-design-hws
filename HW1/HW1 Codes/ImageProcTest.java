/**
 * Created by mpolatcan-gyte_cse on 24.10.2016.
 */
import vpt.Image;
import vpt.algorithms.display.Display2D;
import vpt.algorithms.io.Load;

import java.util.Scanner;

public class ImageProcTest {
    public static void main(String[] args) {
        Image img = Load.invoke("lennaGray.png");
        int choice;

        FilterApp filterApp = new FilterApp(img);
        filterApp.setFilter(new SmoothingFilter());

        Scanner in = new Scanner(System.in);

        while(true) {
            System.out.println("Select filter 1-Edge Detect 2-Smoothing");
            choice = in.nextInt();

            switch (choice) {
                case 1:
                    filterApp.setFilter(new EdgeDetectionFilter());
                    break;
                case 2:
                    filterApp.setFilter(new SmoothingFilter());
                    break;
                default:
                    break;
            }

            if (choice == 1) {
                System.out.println("Select algorithm 1-MorphoInt 2-MorphoExt");
                choice = in.nextInt();

                switch (choice) {
                    case 1:
                        filterApp.setFilterAlgorithm(new MorphoIntAlgorithm());
                        break;
                    case 2:
                        filterApp.setFilterAlgorithm(new MorphoExtAlgorithm());
                        break;
                    default:
                        break;
                }
            } else if (choice == 2) {
                System.out.println("Select algorithm 1-Median 2-Average");
                choice = in.nextInt();

                switch (choice) {
                    case 1:
                        filterApp.setFilterAlgorithm(new MedianAlgorithm());
                        break;
                    case 2:
                        filterApp.setFilterAlgorithm(new AverageAlgorithm());
                        break;
                    default:
                        break;
                }
            }

            Display2D.invoke(filterApp.applyFilter(), "Filtered Image");

        }
    }
}
