import vpt.Image;

/**
 * Created by mpolatcan-gyte_cse on 25.10.2016.
 */
public abstract class Filter {
    protected FilterAlgorithm algorithm;

    public Filter() {

    }

    public Image applyFilter(Image img) {
        return algorithm.applyAlgorithm(img);
    }

    public void setAlgorithm(FilterAlgorithm algorithm) {
        this.algorithm = algorithm;
    }
}
