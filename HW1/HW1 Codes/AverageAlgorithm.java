import vpt.Image;

/**
 * Created by mpolatcan-gyte_cse on 25.10.2016.
 */
public class AverageAlgorithm implements FilterAlgorithm {
    private Image image;

    @Override
    public Image applyAlgorithm(Image image) {
        this.image = image;
        Image result = image.newInstance(false);

        int rows = image.getYDim();
        int cols = image.getXDim();

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; ++j) {
                int average = (getPixelValue(j-2,i-2) + getPixelValue(j-1,i-2) +
                        getPixelValue(j,i-2) + getPixelValue(j+1,i-2) +
                        getPixelValue(j+2,i-2) + getPixelValue(j-2,i-1) +
                        getPixelValue(j-1,i-1) + getPixelValue(j,i-1) +
                        getPixelValue(j+1,i-1) + getPixelValue(j+2,i-1) +
                        getPixelValue(j-2,i) + getPixelValue(j-1,i) +
                        getPixelValue(j,i) + getPixelValue(j+1,i) +
                        getPixelValue(j+2,i) + getPixelValue(j-2,i+1) +
                        getPixelValue(j-1,i+1) + getPixelValue(j,i+1) +
                        getPixelValue(j+1,i+1) + getPixelValue(j+2,i+1) +
                        getPixelValue(j-2,i+2) + getPixelValue(j-1,i+2) +
                        getPixelValue(j,i+2) + getPixelValue(j+1,i+2) +
                        getPixelValue(j+2,i+2)) / 25;
                result.setXYByte(j, i, average);
            }
        }
        return result;
    }

    private int getPixelValue(int x, int y) {
        if (checkXBound(x) && checkYBound(y)) {
            return image.getXYByte(x,y);
        } else {
            return 0;
        }
    }

    private boolean checkXBound(int x) {
        if (x < 0 || x >= image.getXDim()) {
            return false;
        } else {
            return true;
        }
    }

    private boolean checkYBound(int y) {
        if (y < 0 || y >= image.getYDim()) {
            return false;
        } else {
            return true;
        }
    }
}
