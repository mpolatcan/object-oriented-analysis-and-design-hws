import vpt.Image;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by mpolatcan-gyte_cse on 25.10.2016.
 */
public class MedianAlgorithm implements FilterAlgorithm {
    private Image image;

    @Override
    public Image applyAlgorithm(Image image) {
        this.image = image;
        Image result = image.newInstance(false);

        int rows = image.getYDim();
        int cols = image.getXDim();

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; ++j) {
                ArrayList<Integer> intensities = new ArrayList<Integer>();

                intensities.add(getPixelValue(j - 2, i - 2));
                intensities.add(getPixelValue(j - 1, i - 2));
                intensities.add(getPixelValue(j, i - 2));
                intensities.add(getPixelValue(j + 1, i - 2));
                intensities.add(getPixelValue(j + 2, i - 2));
                intensities.add(getPixelValue(j - 2, i - 1));
                intensities.add(getPixelValue(j - 1, i - 1));
                intensities.add(getPixelValue(j, i - 1));
                intensities.add(getPixelValue(j + 1, i - 1));
                intensities.add(getPixelValue(j + 2, i - 1));
                intensities.add(getPixelValue(j - 2, i));
                intensities.add(getPixelValue(j - 1, i));
                intensities.add(getPixelValue(j, i));
                intensities.add(getPixelValue(j + 1, i));
                intensities.add(getPixelValue(j + 2, i));
                intensities.add(getPixelValue(j - 2, i + 1));
                intensities.add(getPixelValue(j - 1, i + 1));
                intensities.add(getPixelValue(j, i + 1));
                intensities.add(getPixelValue(j + 1, i + 1));
                intensities.add(getPixelValue(j + 2, i + 1));
                intensities.add(getPixelValue(j - 2, i + 2));
                intensities.add(getPixelValue(j - 1, i + 2));
                intensities.add(getPixelValue(j, i + 2));
                intensities.add(getPixelValue(j + 1, i + 2));
                intensities.add(getPixelValue(j + 2, i + 2));

                Collections.sort(intensities);

                result.setXYByte(j, i, intensities.get(12));
            }
        }

        return result;
    }


    private int getPixelValue(int x, int y) {
        if (checkXBound(x) && checkYBound(y)) {
            return image.getXYByte(x,y);
        } else {
            return 0;
        }
    }

    private boolean checkXBound(int x) {
        if (x < 0 || x >= image.getXDim()) {
            return false;
        } else {
            return true;
        }
    }

    private boolean checkYBound(int y) {
        if (y < 0 || y >= image.getYDim()) {
            return false;
        } else {
            return true;
        }
    }
}
