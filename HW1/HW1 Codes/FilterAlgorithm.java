import vpt.Image;

/**
 * Created by mpolatcan-gyte_cse on 25.10.2016.
 */
public interface FilterAlgorithm {
    Image applyAlgorithm(Image image);
}
