import vpt.Image;

/**
 * Created by mpolatcan-gyte_cse on 25.10.2016.
 */
public class FilterApp {
    private Filter filter;
    private Image originalImage;

    public FilterApp(Image img) {
        originalImage = img;
    }

    public void setFilter(Filter filter) {
        this.filter = filter;
    }

    public void setFilterAlgorithm(FilterAlgorithm algorithm) {
        filter.setAlgorithm(algorithm);
    }

    public Image applyFilter() {
        return filter.applyFilter(originalImage);
    }
}
